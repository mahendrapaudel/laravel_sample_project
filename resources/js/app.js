
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


window.Vue = require('vue');
import App from "./App.vue";

import router from './router';

import bModal from 'bootstrap-vue/es/components/modal/modal'; 

import axios from 'axios'
import VueAxios from 'vue-axios'


Vue.component('b-modal', bModal);
Vue.use(VueAxios,axios)


import FlashMessage from '@smartweb/vue-flash-message';
Vue.use(FlashMessage);
/**

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('app', require('./App.vue'));



const app = new Vue({
	router,
    el: '#app',
    render: h => h(App)
});
