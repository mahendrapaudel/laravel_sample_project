import axios from 'axios';
export default{
    methods :{
        submitData : function(url,formData,method)
        {
            var absUrl = '';
            console.log(formData);
            var form_data = this.getEncodedFromData(formData);
            form_data.append('_token',this.getCsrfToken());
            //alert(this.getAbsUrl()+"/"+url);
            axios[(method)?method:'post'](this.getAbsUrl()+"/"+url, form_data,this.getConfig).then(response => {
                if(response.status == 200 || response.status == 201 )
                {

                      if(response.data.status == 'error')
                      {
                          var title = 'Validation Error';
                          var message = (response.data.message)?response.data.message:'You have invalid data, please correct as per required!';
                          this.showNotification('error',response.status,title,message);
                          this.setFormDataError(response.data.result);
                      }
                    else if(response.data)
                    {
                        this.tableData = response.data;
                        var title = ((this.scenario)?this.scenario.charAt(0).toUpperCase()+this.scenario.substring(1,this.scenario.length):'Operation')+' Success';
                        var message = (response.data.message)?response.data.message:'Operation has been completed successfully!';
                        if(this.listHandler)
                        {
                            this.listHandler();
                        }

                        if(this.$refs[this.modalId])
                        {
                            this.$refs[this.modalId].close();
                        }
                        this.showNotification('success',response.status,title,message);
                    }

                }
            }).catch(error => {
                if(error.response.status == 401)
                {
                    this.showNotification('error',error.response.status,'Unauthorized Access',error.response.statusText);
                    setTimeout(function(){window.location.href="/login"},15000);
                }
                if(error.response.status == 500)
                {
                    this.showNotification('error',error.response.status,error.response.statusText,error.response.data.message);
                }
            });
        },
        loadList(url,params,method)
        {
            axios.post(this.getAbsUrl()+"/"+url, params,this.getConfig()).then(response => {
                this.responseData = response.data;

            }).catch(error => {
                if(error.response.status == 401)
                {
                    this.showNotification('error',error.response.status,'Unauthorized Access',error.response.statusText);
                    setTimeout(function(){window.location.href="/login"},15000);
                }
                if(error.response.status == 500)
                {
                    this.showNotification('error',error.response.status,error.response.statusText,error.response.data.message);
                }
            });
        },
        getInfo(url,params,method){
            axios[(method)?method:'get'](this.getAbsUrl()+"/"+url, params,this.getConfig()).then(response => {
                this.setFormData(response.data.result);
                console.log('response-data'+response.data.result);
                if(this.$refs[this.modalId])
                {
                    this.$refs[this.modalId].open();
                }
            }).catch(error => {
                if(error.response.status == 401)
                {
                    this.showNotification('error',error.response.status,'Unauthorized Access',error.response.statusText);
                    setTimeout(function(){window.location.href="/login"},15000);
                }
                if(error.response.status == 500)
                {
                    this.showNotification('error',error.response.status,error.response.statusText,error.response.data.message);
                }
            });
        },
        deleteRecord(url,params,method)
        {
            axios[(method)?method:'delete'](this.getAbsUrl()+"/"+url, params,this.getConfig()).then(response => {
                this.hideDeleteModal();
                if(this.listHandler)
                {
                    this.listHandler();
                }
            }).catch(error => {
                if(error.response.status == 401)
                {
                    this.hideDeleteModal();
                    this.showNotification('error',error.response.status,'Unauthorized Access',error.response.statusText);
                    setTimeout(function(){window.location.href="/login"},15000);
                }
                if(error.response.status == 500)
                {
                    this.hideDeleteModal();
                    this.showNotification('error',error.response.status,error.response.statusText,error.response.data.message);
                }
            });
        },
        loadSelect(selectObj)
        {
          //alert(selectObj.loadUrl);
            if(selectObj.loadUrl)
            {
                axios.post(this.getAbsUrl()+"/"+selectObj.loadUrl, selectObj.params,this.getConfig()).then(response => {
                    selectObj.options = response.data.result;
                }).catch(error => {
                    if(error.response.status == 401)
                    {
                        this.showNotification('error',error.response.status,'Unauthorized Access',error.response.statusText);
                        setTimeout(function(){window.location.href="/login"},15000);
                    }
                    if(error.response.status == 500)
                    {
                        this.showNotification('error',error.response.status,error.response.statusText,error.response.data.message);
                    }
                });
            }
        },
        setFormData(jsonObject)
        {
            var currentInstance = this;

            this.formData.fields.forEach(function(field){
                if(!field.isGroupField)
                {
                    field.value = (jsonObject[field.name] && field.type != 'file')?jsonObject[field.name]:'';

                    if(field.value =='' && field.defaultValue)
                    {
                      field.value = field.defaultValue;
                    }
                    if(field.type == 'date')
                    {
                        var dateParts = field.value.split(" ");
                        field.value = dateParts[0];
                    }

                }
                else{
                    field.fields.forEach(function(subfields){
                        if(!subfields.isGroupField)
                        {

                            subfields.value = (jsonObject[subfields.name] && subfields.type !='file')?jsonObject[subfields.name]:'';

                            if(subfields.value =='' && subfields.defaultValue)
                            {
                              subfields.value = subfields.defaultValue;
                            }
                            if(subfields.type == 'date')
                            { //alert('hrllp'+dateParts)
                              var dateParts = subfields.value.split(" ");
                              subfields.value = dateParts[0];
                              //alert(subfields.value);
                            }

                        }
                        else
                        {
                            subfields.fields.forEach(function(subfield){
                                subfield.value = (jsonObject[subfield.name] && subfield.type !='file')?jsonObject[subfield.name]:'';
                                if(subfield.value =='' && subfield.defaultValue)
                                {
                                  subfield.value = subfield.defaultValue;
                                }

                            });
                        }

                    });
                }
            });
        },
        setFormDataError(jsonObject)
        {
            var currentInstance = this;
            this.formData.fields.forEach(function(field){
                if(!field.isGroupField)
                {
                    field.backendError = (jsonObject[field.name])?jsonObject[field.name].join('\n'):'';
                    var value = field.value;
                    field.value = 'hello';
                    field.value = value;
                }
                else{
                    field.fields.forEach(function(subfields){
                        if(!subfields.isGroupField)
                        {
                            subfields.backendError = (jsonObject[subfields.name])?jsonObject[subfields.name].join('\n'):'';
                            var value = subfields.value;
                            subfields.value = 'hello';
                            subfields.value = value;
                        }
                        else
                        {
                            subfields.fields.forEach(function(subfield){
                                subfield.backendError = (jsonObject[subfield.name])?jsonObject[subfield.name].join('\n'):'';
                                var value = subfield.value;
                                subfield.value = 'hello';
                                subfield.value = value;
                            });
                        }

                    });
                }
            });

        },
        getCsrfToken()
        {
            var token = '';
            var metaTags = document.getElementsByTagName("meta");
            for(var index=0;index<metaTags.length;index++)
            {
                if(metaTags[index].getAttribute("name")=='_csrf')
                {
                    token = metaTags[index].getAttribute("content");
                }
            }
            return token;
        },
        getAbsUrl()
        {
            var absUrl = '';
            var metaTags = document.getElementsByTagName("meta");
            for(var index=0;index<metaTags.length;index++)
            {
                if(metaTags[index].getAttribute('name') == "_url")
                {
                    absUrl = metaTags[index].getAttribute('content');
                }
            }
            return absUrl;
        },
        getEncodedFromData(formData)
        {
            var keys = Object.keys(formData);
            var form_data = new FormData();
            keys.forEach(function(key,index){
                form_data.append(key,formData[key]);
            });
            return form_data;
        },
        getConfig()
        {
            return  {
                        headers:{'Access-Control-Allow-Origin': '*',
                                    'Content-Type':undefined
                                }
                    };
        },
        showNotification(type,status,title,message)
        {
            var icons = {  error:'/img/warning.svg',
                        info : '/img/info.svg',
                        success : '/img/success.svg',
                        warning : '/img/light_bulb.svg'
                    }

            this.flashMessage[type]({
                title: title,
                message: message,
                icon : icons[type],
                time: 15000,
            });
        }
    }
}
