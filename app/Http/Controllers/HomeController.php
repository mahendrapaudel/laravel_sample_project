<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $user = \Auth::User();
        return view('home',['userInfo' => ['userID'=>$user->user_id,'userName'=>$user->name,'email'=>$user->email,'dashboard_type'=>$user->dashboard_type]]);
    }
}
