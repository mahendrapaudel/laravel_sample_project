<?php

namespace App\Http\Controllers\Api\V1;
use App\Client;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Collection;

/**
 * @group Clients Management
 *
 * APIs for managing clients
 */
class ClientController extends Controller
{
  /**
         * @bodyParam {"page":1,"limit":5,"limitOptions":[5,10,15,20],"search":{"field":"name","value":""}} object required name.
         * @response {
         *  "status": "success",
         *  "result": {
         *   "total": 12,
         *   "rows" : [{
         *        "client_id": 2,
         *        "name": null,
         *        "vat_id": "22",
         *        "address_1": "kathmandu",
         *        "address_2": null,
         *        "address_3": null,
         *        "zip_code": 34543,
         *        "city": "ktm",
         *        "national_code": "fdsafs",
         *        "invoice_address_1": "fasdfdasf",
         *        "invoice_address_2": null,
         *        "invoice_address_3": null,
         *        "invoice_zipcode": 2222,
         *        "invoice_city": "fasdf",
         *        "invoice_national_code": "432432",
         *        "deleted_at": null,
         *        "created_at": "2018-12-06 15:11:42",
         *        "updated_at": "2018-12-06 15:11:42"
         *    },
         *    {
         *        "client_id": 3,
         *        "name": null,
         *        "vat_id": "22",
         *        "address_1": "kathmandu",
         *        "address_2": null,
         *        "address_3": null,
         *        "zip_code": 34543,
         *        "city": "ktm",
         *        "national_code": "fdsafs",
         *        "invoice_address_1": "fasdfdasf",
         *        "invoice_address_2": null,
         *        "invoice_address_3": null,
         *        "invoice_zipcode": 2222,
         *        "invoice_city": "fasdf",
         *        "invoice_national_code": "432432",
         *        "deleted_at": null,
         *        "created_at": "2018-12-06 15:14:23",
         *        "updated_at": "2018-12-06 15:14:23"
         *    }]
         *  },
         *  "messages": null
         * }
        */

       public function index(Request $request, Client $client)
       {
         $client = $client->getClients($request);
         return response()->json([
            'status' => 'success',
            'result' => [
                'total' => $client->total(),
                'rows' => $client->items()
            ],
            'messages' => null
          ]);
       }


     /**
       * @bodyParam name string required name.
       * @bodyParam vat_id string required vat_id.
       * @bodyParam address_1 string required address_1.
       * @bodyParam address_2 string optional address_2.
       * @bodyParam address_3 string required address_3.
       * @bodyParam zip_code string required zip_code.
       * @bodyParam city string required city.
       * @bodyParam national_code string required national_code.
       * @bodyParam invoice_address_1 string required invoice_address_1.
       * @bodyParam invoice_address_2 string required invoice_address_2.
       * @bodyParam invoice_address_3 string required invoice_address_3.
       * @bodyParam invoice_zipcode string required invoice_zipcode.
       * @bodyParam invoice_city string required invoice_city.
       * @bodyParam invoice_national_code string required invoice_national_code.
       * @response {
       *  "status": "success",
       *  "result": {
       *  "client_id": 5,
       *  "name": null,
       *  "vat_id": "d23424",
       *  "address_1": "Kathmandu",
       *  "address_2": null,
       *  "address_3": null,
       *  "zip_code": 123,
       *  "city": "kathmandu",
       *  "national_code": "no-123",
       *  "invoice_address_1": "Kathmandu -lalitpur",
       *  "invoice_address_2": null,
       *  "invoice_address_3": null,
       *  "invoice_zipcode": 123,
       *  "invoice_city": "Lalitpur",
       *  "invoice_national_code": "23233",
       *  "deleted_at": null,
       *  "created_at": "2018-12-06 15:26:30",
       *  "updated_at": "2018-12-06 15:26:30"
       *   },
       *  "messages": null
       * }
      */
      public function store(Request $request)
      {
        $rules = [
            'name' => 'required',
            'vat_id' => 'required',
            'address_1' => 'required',
            'zip_code' => 'required|integer',
            'city' => 'required',
            'national_code' => 'required',
            'invoice_address_1' => 'required',
            'invoice_zipcode'  => 'required|integer',
            'invoice_city'  => 'required',
            'invoice_national_code'  => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
          if (!$validator->fails()) {
            $client = new Client;
            $client->name =  $request->name;
            $client->vat_id =  $request->vat_id;
            $client->address_1 =  $request->address_1;
            $client->address_2 =  $request->address_2;
            $client->address_3 =  $request->address_3;
            $client->zip_code =  $request->zip_code;
            $client->city =  $request->city;
            $client->national_code =  $request->national_code;
            $client->invoice_address_1 =  $request->invoice_address_1;
            $client->invoice_address_2 =  $request->invoice_address_2;
            $client->invoice_address_3 =  $request->invoice_address_3;
            $client->invoice_zipcode =  $request->invoice_zipcode;
            $client->invoice_city =  $request->invoice_city;
            $client->invoice_national_code =  $request->invoice_national_code;
            $client->save();
            return response()->json([
                  'status' => 'success',
                  'result' => $client,
                  'message' => 'Client Created!'
                  ], 201);
              } else {
                return response()->json([
                  'status' => 'error',
                  'result' => $validator->messages(),
                  'message' => null
                ]);
              }
      }

      /**
      * @bodyParam client_id int required the ID of the Client
      * @response {
      *  "status": "success",
      *  "result": {
      *  "client_id": 5,
      *  "name": null,
      *  "vat_id": "d23424",
      *  "address_1": "Kathmandu",
      *  "address_2": null,
      *  "address_3": null,
      *  "zip_code": 123,
      *  "city": "kathmandu",
      *  "national_code": "no-123",
      *  "invoice_address_1": "Kathmandu -lalitpur",
      *  "invoice_address_2": null,
      *  "invoice_address_3": null,
      *  "invoice_zipcode": 123,
      *  "invoice_city": "Lalitpur",
      *  "invoice_national_code": "23233",
      *  "deleted_at": null,
      *  "created_at": "2018-12-06 15:26:30",
      *  "updated_at": "2018-12-06 15:26:30"
      *   },
      *  "messages": null
      * }
     */
    public function show(Client $client)
    {
      return response()->json([
            'status' => 'success',
            'result' => $client,
            'messages' => null
          ], 200);
    }


    /**
      * @bodyParam name string required The title of the post.
      * @bodyParam email email required The title of the post.
      * @bodyParam date_of_birth date optional  The type of post to create. Defaults to 'textophonious'.
      * @bodyParam client_id int required the ID of the Client
      * @bodyParam tracker_type_id int required  the ID of the Tracker Type
      * @bodyParam dashboard_type int optional
      * @bodyParam password string required Password
      * @response {
      *  "status": "success",
      *  "result": {
      *  "client_id": 5,
      *  "name": null,
      *  "vat_id": "d23424",
      *  "address_1": "Kathmandu",
      *  "address_2": null,
      *  "address_3": null,
      *  "zip_code": 123,
      *  "city": "kathmandu",
      *  "national_code": "no-123",
      *  "invoice_address_1": "Kathmandu -lalitpur",
      *  "invoice_address_2": null,
      *  "invoice_address_3": null,
      *  "invoice_zipcode": 123,
      *  "invoice_city": "Lalitpur",
      *  "invoice_national_code": "23233",
      *  "deleted_at": null,
      *  "created_at": "2018-12-06 15:26:30",
      *  "updated_at": "2018-12-06 15:26:30"
      *   },
      *  "message": 'Client Updated!'
      * }
     */
    public function update( Client $client, Request $request )
    {
      $rules = [
          'name' => 'required',
          'vat_id' => 'required',
          'address_1' => 'required',
          'zip_code' => 'required|integer',
          'city' => 'required',
          'national_code' => 'required',
          'invoice_address_1' => 'required',
          'invoice_zipcode'  => 'required|integer',
          'invoice_city'  => 'required',
          'invoice_national_code'  => 'required',
      ];


      $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails()) {
          $client->name =  $request->name;
          $client->vat_id =  $request->vat_id;
          $client->address_1 =  $request->address_1;
          $client->address_2 =  $request->address_2;
          $client->address_3 =  $request->address_3;
          $client->zip_code =  $request->zip_code;
          $client->city =  $request->city;
          $client->national_code =  $request->national_code;
          $client->invoice_address_1 =  $request->invoice_address_1;
          $client->invoice_address_2 =  $request->invoice_address_2;
          $client->invoice_address_3 =  $request->invoice_address_3;
          $client->invoice_zipcode =  $request->invoice_zipcode;
          $client->invoice_city =  $request->invoice_city;
          $client->invoice_national_code =  $request->invoice_national_code;
          $client->update();
          return response()->json([
                'status' => 'success',
                'result' => $client,
                'message' => 'Client Updated!'
              ], 200);
        } else {
          return response()->json([
              'status' => 'error',
              'result' => $validator->messages(),
              'message' => null
          ]);
      }
    }

    /**
      * @bodyParam client_id int required the ID of the Client
      * @response {
      *  "status": "success",
      *  "result": "null",
      *  "message": 'Client Deleted!'
      * }
     */

    public function destroy(Client $client)
    {
      $client->delete();
      return response()->json([
            'status' => 'success',
            'result' => 'null',
            'message' => 'Client Deleted!'
          ], 200);
    }

    /**
      * @bodyParam client_id int required the ID of the Client
      * @response {
      * {"status":"success",
      * "result":[
      * {"value":14,"caption":"new customer title"},
      * {"value":13,"caption":"dsafdaf"},
      * {"value":12,"caption":"jjkj edited"},
      * {"value":2,"caption":"The yy Group"}
      * ],
      * "messages":null
      * }
      * }
     */

    public function autocomplete(Client $client)
    {
      $clients=$client->getClientsAll();
      return response()->json([
            'status' => 'success',
            'result' => $clients,
            'messages' => null
          ], 200);

    }
}
