<?php

namespace App\Http\Controllers\Api\V1;
use App\Magazine;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
/**
 * @group magazines Management
 *
 * APIs for managing magazines
 */
class MagazineController extends Controller
{
  /**
         * @bodyParam {"page":1,"limit":5,"limitOptions":[5,10,15,20],"search":{"field":"name","value":""}} object   required name.
          *  @response {
          * "status": "success",
          * "result": {
          * "total": 8,
          * "rows": [
          *     {
          *       "magazine_id": 3,
          *       "client_id": null,
          *       "api_key": "23233ewrwr",
          *       "name": "last test",
          *       "description": "this is desc",
          *       "logo": null,
          *       "deleted_at": null,
          *       "created_at": "2018-12-13 09:39:45",
          *       "updated_at": "2018-12-13 09:39:45"
          *     },
          *     {
          *       "magazine_id": 12,
          *       "client_id": 2,
          *       "api_key": "23233ewrwr",
          *       "name": "last test",
          *       "description": "this is description",
          *       "logo": null,
          *       "deleted_at": null,
          *       "created_at": "2018-12-13 10:32:24",
          *       "updated_at": "2018-12-13 10:32:24"
          *     }
          *   ]
          *  },
          *  "messages": null
          *  }
        */

       public function index(Request $request, Magazine $magazine)
       {
         $magazine = $magazine->getMagazines($request);
          return response()->json([
            'status' => 'success',
            'result' => [
                'total' => $magazine->total(),
                'rows' => $magazine->items()
            ],
            'messages' => null
          ]);
       }


     /**
       * @bodyParam name string required name.
       * @bodyParam client_id int required client_id, is a foreign key.
       * @bodyParam api_key int required api_key.
       * @bodyParam description string optional Description of Magazine.
       * @bodyParam logo file optional Logo of Magazine.
       * @response {
       *   "status": "success",
       *   "result": {
       *        "name": "last test",
       *        "api_key": "23233ewrwr",
       *        "client_id": "2",
       *        "description": "this is description",
       *        "logo": "magazine_logo_url",
       *        "updated_at": "2018-12-13 10:46:08",
       *        "created_at": "2018-12-13 10:46:08",
       *        "magazine_id": 13
       *             },
       *       "message": 'Magazine Created!'
       *     }
      */
      public function store(Request $request)
      {
        $rules = [
            'name' => 'required',
            'client_id' => 'required',
            'api_key' => 'required',
            'logo1' => 'mimes:jpeg,bmp,png,ico'
        ];



        $validator = Validator::make($request->all(), $rules);
          if (!$validator->fails()) {
            if($request->file('logo1')) {

             $file = $request->file('logo1');
             // generate a new filename. getClientOriginalExtension() for the file extension
              $filename = 'logo-image-' . time() . '.' . $file->getClientOriginalExtension();

              // save to storage/app/photos as the new $filename
              //  $path_file = $file->storeAs('photos', $filename);'file.txt', 'Contents'
              $contents=file_get_contents($request->file('logo1')->getRealPath());
              $path=Storage::disk('public_uploads')->put($filename, $contents);
              if(!$path) {
              return false;
              }


              $path=$filename;
            }else {
              $path="";
            }
            $magazine = new Magazine;
            $magazine->name =  $request->name;
            $magazine->api_key =  $request->api_key;
            $magazine->client_id =  $request->client_id;
            $magazine->description =  $request->description;
            $magazine->logo =  $path;
            $magazine->save();
            return response()->json([
                  'status' => 'success',
                  'result' => $magazine,
                  'message' => 'Magazine Created!'
                  ], 201);
              } else {
                return response()->json([
                  'status' => 'error',
                  'result' => $validator->messages(),
                  'messages' => null
                ]);
              }
      }

      /**
      * @bodyParam magazine_id int required the ID of the magazine
      * @response {
      *  "status": "success",
      *  "result": {
      *    "magazine_id": 13,
      *    "client_id": 2,
      *    "api_key": "23233ewrwr",
      *     "name": "last test",
      *     "description": "this is description",
      *     "logo": null,
      *     "deleted_at": null,
      *     "created_at": "2018-12-13 10:46:08",
      *     "updated_at": "2018-12-13 10:46:08"
      *          },
      *     "messages": null
      *    }
     */
    public function show(Magazine $magazine)
    {
      return response()->json([
            'status' => 'success',
            'result' => $magazine,
            'messages' => null
          ], 200);
    }


    /**
      * @bodyParam magazine_id integer required Id of magazine which needs to updated.
      * @bodyParam name string required The title of the magazine.
      * @bodyParam api_key string required The title of the magazine.
      * @bodyParam client_id integer required, is foreign key.
      * @bodyParam description string required for the description of the magazine
      * @bodyParam logo file optional for the logo of the magazine
      * @response {
      *      "status": "success",
      *      "result": {
      *          "magazine_id": 13,
      *          "client_id": "2",
      *          "api_key": "454545345",
      *          "name": "magazinename23456546546",
      *          "description": "description34",
      *          "logo": null,
      *          "deleted_at": null,
      *          "created_at": "2018-12-13 10:46:08",
      *          "updated_at": "2018-12-13 10:51:08"
      *          },
      *      "message": 'Magazine Updated!'
      *   }
     */
    public function update( Magazine $magazine, Request $request )
    {
      $rules1 = [
          'name' => 'required',
          'api_key' => 'required',
          'client_id' => 'required',
          'description' => 'required'
      ];

      $rules2=[];
      if($request->file('logo1')) {
       $rules2 = ['logo1' => 'mimes:jpeg,bmp,png,ico'];
     }

      $rules=array_merge($rules1,$rules2);


      $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails()) {
          if($request->file('logo1')) {
            $file = $request->file('logo1');
            // generate a new filename. getClientOriginalExtension() for the file extension
             $filename = 'logo-image-' . time() . '.' . $file->getClientOriginalExtension();

             $contents=file_get_contents($request->file('logo1')->getRealPath());
             $path=Storage::disk('public_uploads')->put($filename, $contents);
             if(!$path) {
             return false;
             }
             $path=$filename;
             $old_image=$magazine->logo;
           }else {
             $path="";
             $old_image="";
           }

          $magazine->name   =  $request->name;
          $magazine->api_key =  $request->api_key;
          $magazine->client_id =  $request->client_id;
          $magazine->description =  $request->description;
          if($path){
          $magazine->logo =  $path;
        }

          $magazine->update();

        if($old_image) {
          Storage::disk('public_uploads')->delete($old_image);
        }

          return response()->json([
                'status' => 'success',
                'result' => $magazine,
                'message' => 'Magazine Updated!'
              ], 200);
        } else {
          return response()->json([
              'status' => 'error',
              'result' => $validator->messages(),
              'messages' => null
          ]);
      }
    }

    /**
      * @bodyParam magazine_id int required the ID of the magazine
      * @response {
      *  "status": "success",
      *  "result": "null",
      *  "message": 'Magazine Deleted!'
      * }
     */

    public function destroy(Magazine $magazine)
    {
      $logo=$magazine->logo;
      $magazine->delete();
      Storage::disk('public_uploads')->delete($logo);
      return response()->json([
            'status' => 'success',
            'result' => 'null',
            'message' => 'Magazine Deleted!'
          ], 200);
    }

    /**
           * @bodyParam {"page":1,"limit":5,"limitOptions":[5,10,15,20],"search":{"field":"name","value":""}} object   required name.
            *  @response {
            * "status": "success",
            * "result": {
            * "total": 8,
            * "rows": [
            *     {
            *       "magazine_id": 3,
            *       "client_id": null,
            *       "api_key": "23233ewrwr",
            *       "name": "last test",
            *       "description": "this is desc",
            *       "logo": null,
            *       "deleted_at": null,
            *       "created_at": "2018-12-13 09:39:45",
            *       "updated_at": "2018-12-13 09:39:45"
            *     },
            *     {
            *       "magazine_id": 12,
            *       "client_id": 2,
            *       "api_key": "23233ewrwr",
            *       "name": "last test",
            *       "description": "this is description",
            *       "logo": null,
            *       "deleted_at": null,
            *       "created_at": "2018-12-13 10:32:24",
            *       "updated_at": "2018-12-13 10:32:24"
            *     }
            *   ]
            *  },
            *  "messages": null
            *  }
          */

         public function get_magazines_by_client(Request $request, Magazine $magazine)
         {
          $client_id=$request->client_id;
           $magazine = $magazine->getMagazinesClients($request,$client_id);
            return response()->json([
              'status' => 'success',
              'result' => [
                  'total' => $magazine->total(),
                  'rows' => $magazine->items()
              ],
              'messages' => null
            ]);
         }
}
