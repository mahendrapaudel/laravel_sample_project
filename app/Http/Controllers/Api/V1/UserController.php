<?php

namespace App\Http\Controllers\Api\V1;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Hash;
use Auth;



/**
 * @group User management
 *
 * APIs for managing users
 */
class UserController extends Controller
{
/**
       * @bodyParam {"page":1,"limit":5,"limitOptions":[5,10,15,20],"search":{"field":"name","value":""}} object required name.
       * @response {
       *  "status": "success",
       *  "result": {
       *   "total": 12,
       *   "rows" : [{
       *        "user_id": 2,
       *        "name": veshraj,
       *        "email": "veshraj@globdig.com",
       *        "email_verified_at": null,
       *        "date_of_birth": null,
       *        "client_id": null,
       *        "tracker_type_id": null,
       *        "dashboard_type": null,
       *        "deleted_at": null,
       *        "created_at": "2018-12-06 15:11:42",
       *        "updated_at": "2018-12-06 15:11:42"
       *    },
       *    {
       *        "user_id": 3,
       *        "name": mahendra,
       *        "email": "mahendra@globdig.com",
       *        "email_verified_at": null,
       *        "date_of_birth": null,
       *        "client_id": null,
       *        "tracker_type_id": null,
       *        "dashboard_type": null,
       *        "deleted_at": null,
       *        "created_at": "2018-12-06 15:11:42",
       *        "updated_at": "2018-12-06 15:11:42"
       *    }]
       *  },
       *  "messages": null
       * }
      */


       public function index(Request $request, User $user)
       {
         $user = $user->getUsers($request);
          return response()->json([
            'status' => 'success',
            'result' => [
                'total' => $user->total(),
                'rows' => $user->items()
            ],
            'messages' => null
          ]);
       }

       /**
         * @bodyParam name string required name.
         * @bodyParam email string required email.
         * @bodyParam password string required password.
         * @bodyParam conPassword string required conPassword.
         * @bodyParam dashboard_type string required dashboard_type.
         * @bodyParam date_of_birth string required date_of_birth.
         * @response {
         * "status":"success",
         * "result":{
         * "name":"The yy Group",
         * "email":"mm@ddsaf.com",
         * "dashboard_type":"1",
         * "date_of_birth":"2010-2-12",
         * "updated_at":"2018-12-19 08:13:18",
         * "created_at":"2018-12-19 08:13:18",
         * "user_id":37
         *},
         *"messages":'User Created!'
         *}
        */
    public function store(Request $request)
    {
      $rules = [
          'name' => 'required',
          'email' => 'required|email',
          'password' => 'required',
          'conPassword' => 'required|same:password',
          'dashboard_type' => 'required',
          'date_of_birth' => 'required',
      ];

      $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails()) {
          $user = new User;
          $user->name =  $request->name;
          $user->email =  $request->email;
          $user->password = $request->password;
          $user->dashboard_type =  $request->dashboard_type;
          $user->date_of_birth =  $request->date_of_birth;
          $user->client_id=$request->client_id;

          $user->save();
          return response()->json([
                'status' => 'success',
                'result' => $user,
                'message' => 'User Created!'
                ], 201);
            } else {
              return response()->json([
                'status' => 'error',
                'result' => $validator->messages(),
                'messages' => null
              ]);
            }
    }

    /**
    * @bodyParam client_id int required the ID of the User
    * @response {
    *  "status": "success",
    *  "result": {
    *  "client_id": 5,
    *  "name": null,
    *  "vat_id": "d23424",
    *  "address_1": "Kathmandu",
    *  "address_2": null,
    *  "address_3": null,
    *  "zip_code": 123,
    *  "city": "kathmandu",
    *  "national_code": "no-123",
    *  "invoice_address_1": "Kathmandu -lalitpur",
    *  "invoice_address_2": null,
    *  "invoice_address_3": null,
    *  "invoice_zipcode": 123,
    *  "invoice_city": "Lalitpur",
    *  "invoice_national_code": "23233",
    *  "deleted_at": null,
    *  "created_at": "2018-12-06 15:26:30",
    *  "updated_at": "2018-12-06 15:26:30"
    *   },
    *  "messages": null
    * }
   */
  public function show(User $user)
  {
    return response()->json([
          'status' => 'success',
          'result' => $user,
          'messages' => null
        ], 200);
  }


      /**
        * @bodyParam name string required The title of the post.
        * @bodyParam email email required The title of the post.
        * @bodyParam date_of_birth date optional  The type of post to create. Defaults to 'textophonious'.
        * @bodyParam client_id int required the ID of the User
        * @bodyParam tracker_type_id int required  the ID of the Tracker Type
        * @bodyParam dashboard_type int optional
        * @bodyParam password string required Password
        * @response {
        * "status":"success",
        * "result":{
        * "name":"The yy Group",
        * "email":"mm@ddsaf.com",
        * "dashboard_type":"1",
        * "date_of_birth":"2010-2-12",
        * "updated_at":"2018-12-19 08:13:18",
        * "created_at":"2018-12-19 08:13:18",
        * "user_id":37
        *},
        *"message":'User Updated!'
        * }
       */
      public function update( User $user, Request $request )
      {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'dashboard_type' => 'required',
            'date_of_birth' => 'required',
        ];


        $validator = Validator::make($request->all(), $rules);
          if (!$validator->fails()) {

            $user->name =  $request->name;
            $user->email =  $request->email;
            $user->dashboard_type =  $request->dashboard_type;
            $user->date_of_birth =  $request->date_of_birth;
            $user->client_id=$request->client_id;

            $user->update();
            return response()->json([
                  'status' => 'success',
                  'result' => $user,
                  'message' => 'User Updated!'
                ], 200);
          } else {
            return response()->json([
                'status' => 'error',
                'result' => $validator->messages(),
                'messages' => null
            ]);
        }
      }

      /**
        * @bodyParam user_id int required the ID of the User
        * @response {
        *  "status": "success",
        *  "result": "null",
        *  "messages": null
        * }
       */

      public function destroy(User $user)
      {
        $user->delete();
        return response()->json([
              'status' => 'success',
              'result' => 'null',
              'messages' => null
            ], 200);
      }

/**
     * @bodyParam {"page":1,"limit":5,"limitOptions":[5,10,15,20],"search":{"field":"name","value":""}} object required name.
     * @response {
     *  "status": "success",
     *  "result": {
     *   "total": 12,
     *   "rows" : [{
     *        "user_id": 2,
     *        "name": veshraj,
     *        "email": "veshraj@globdig.com",
     *        "email_verified_at": null,
     *        "date_of_birth": null,
     *        "clinet_id": null,
     *        "tracker_type_id": null,
     *        "dashboard_type": null,
     *        "deleted_at": null,
     *        "created_at": "2018-12-06 15:11:42",
     *        "updated_at": "2018-12-06 15:11:42"
     *    },
     *    {
     *        "user_id": 3,
     *        "name": mahendra,
     *        "email": "mahendra@globdig.com",
     *        "email_verified_at": null,
     *        "date_of_birth": null,
     *        "clinet_id": null,
     *        "tracker_type_id": null,
     *        "dashboard_type": null,
     *        "deleted_at": null,
     *        "created_at": "2018-12-06 15:11:42",
     *        "updated_at": "2018-12-06 15:11:42"
     *    }]
     *  },
     *  "messages": null
     * }
    */


     public function get_users_by_client(Request $request, User $user)
     {
       $client_id=$request->client_id;
       $user = $user->getUsersClients($request,$client_id);
        return response()->json([
          'status' => 'success',
          'result' => [
              'total' => $user->total(),
              'rows' => $user->items()
          ],
          'messages' => null
        ]);
     }

    public function logout(Request $request) {
      Auth::logout();
      return response()->json([
        'status' => 'success',
        'messages' => null
      ]);
    }




}
