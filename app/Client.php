<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{

    protected $table = 'clients';


     protected $fillable = [
         'name', 'vat_id', 'address_1','address_2', 'address_3', 'zip_code', 'city', 'national_code', 'invoice_address_1',
         'invoice_address_2', 'invoice_address_3', 'invoice_zipcode', 'invoice_city', 'invoice_national_code',
     ];



     /**
    * Change Primary Key
    *
    *
    */
    protected $primaryKey = 'client_id';

    public function replaceField($field, $fields = [])
    {
        if (in_array($field, $fields)) {
            return $fields[$field];
        }

        return $field;
    }

    /**
     * Get List of Clients
     * @access  public
     * @param
     * @return  json(array)
     */

    public function getClients($request)
    {
        $clients = $this->select(['*']);
        if (!empty($request->search['field'])) {
            $searchField = $request->search['field'];
            $searchValue = $request->search['value'];
            $clients->where($searchField, 'like', '%' . $searchValue . '%');
        }
        $clients->orderBy('clients.client_id', 'desc');
        return $clients->paginate($request->limit);
    }

    /**
     * Get List of All Clients
     * @access  public
     * @param
     * @return  json(array)
     */
    public function getClientsAll()
    {
        $clients = $this->select('client_id as value','name as caption');
        $clients->orderBy('clients.client_id', 'desc');
        return $clients->get();
    }

}
