<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
      protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','date_of_birth', 'client_id', 'tracker_type_id', 'dashboard_type',
    ];

     /**
     * Change Primary Key
     *
     *
     */
    protected $primaryKey = 'user_id';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get List of Users
     * @access  public
     * @param
     * @return  json(array)
     */

    public function getUsers($request)
    {
        $users = $this->select(['users.*','clients.name as clientName']);
        if (!empty($request->search['field'])) {
            $searchField = $request->search['field'];
            $searchValue = $request->search['value'];
            $users->where($searchField, 'like', '%' . $searchValue . '%');
        }
        $users->join('clients', 'clients.client_id', '=', 'users.client_id');
        $users->orderBy('users.user_id', 'desc');
        return $users->paginate($request->limit);
    }

    /**
     * Get List of Users of Clients
     * @access  public
     * @param
     * @return  json(array)
     */

    public function getUsersClients($request,$client_id)
    {
        $users = $this->select(['*']);
        if (!empty($request->search['field'])) {
            $searchField = $request->search['field'];
            $searchValue = $request->search['value'];
            $users->where($searchField, 'like', '%' . $searchValue . '%');
        }
        $users->orderBy('users.user_id', 'desc');
        $users->where('client_id',$client_id);
        return $users->paginate($request->limit);
    }
}
