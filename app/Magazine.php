<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Magazine extends Model
{

    protected $table = 'magazines';
    protected $primaryKey = 'magazine_id';
    protected $fillable = ['magazine_id','client_id','api_key','name','description','logo'];



    public function replaceField($field, $fields = [])
    {
        if (in_array($field, $fields)) {
            return $fields[$field];
        }

        return $field;
    }

    /**
     * Get List of Magazines
     * @access  public
     * @param
     * @return  json(array)
     */

    public function getMagazines($request)
    {
        $magazines = $this->select(['magazines.*','clients.name as clientName']);
        if (!empty($request->search['field'])) {
            $searchField = $request->search['field'];
            $searchValue = $request->search['value'];
            $magazines->where($searchField, 'like', '%' . $searchValue . '%');
        }
        $magazines->join('clients', 'clients.client_id', '=', 'magazines.client_id');
        $magazines->orderBy('magazines.magazine_id', 'desc');

        return $magazines->paginate($request->limit);
    }

    public function getMagazinesClients($request,$client_id)
    {
        $magazines = $this->select(['*']);
        if (!empty($request->search['field'])) {
            $searchField = $request->search['field'];
            $searchValue = $request->search['value'];
            $magazines->where($searchField, 'like', '%' . $searchValue . '%');
        }
        $magazines->orderBy('magazines.client_id', 'desc');
        $magazines->where('client_id',$client_id);
        return $magazines->paginate($request->limit);
    }

}
