<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;

class MagazineTest extends TestCase
{    
    public function testCreateMagazineWithMiddleware()
    {
      $user = factory(\App\User::class)->create();
      $client =factory(\App\Client::class)->create();
            $data = [
              "name"=> 'test magazine name',
              "api_key"=> "23233ewrwr",
              "client_id"=> $client->client_id,
              "description"=>null,
              "logo"=> UploadedFile::fake()->image('image.jpg'),
                   ];

        $response = $this->json('POST', '/api/v1/magazine',$data);
        $response->assertStatus(401);
        $response->assertJson(['message' => "Unauthenticated."]);
    }
    public function testCreateMagazine()
    {
      $user = factory(\App\User::class)->create();
      $client =factory(\App\Client::class)->create();

      $data = [
        "name"=> 'test magazine name',
        "api_key"=> "23233ewrwr",
        "client_id"=> $client->client_id,
        "description"=>null,
        "logo1"=> UploadedFile::fake()->image('image.jpg'),
             ];
            $response = $this->actingAs($user)->json('POST', '/api/v1/magazine',$data);
            $response->assertStatus(201);
            $response->assertJson(['status' => true]);
            $response->assertJson(['message' => "Magazine Created!"]);

      }

      public function testGettingAllMagazines()
      {
        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user)->json('POST', '/api/v1/magazines');
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
              'result'=>[
                          'total',
                          'rows'=>
                                    [
                                      '*'=>[
                                        "magazine_id",
                                        "client_id",
                                        "api_key",
                                        "name",
                                        "description",
                                        "logo",
                                        "deleted_at",
                                        "created_at",
                                        "updated_at"
                                        ]
                                    ]
                        ]
            ]
       );
      }

      public function testUpdateMagazine()
      {
      $client =factory(\App\Client::class)->create();
      $user = factory(\App\User::class)->create();
      $response = $this->actingAs($user)->json('POST', '/api/v1/magazines');
      $response->assertStatus(200);
      $magazine = $response->getData()->result->rows[0];
      $update = $this->actingAs($user)->json('PUT', '/api/v1/magazine/'.$magazine->magazine_id,[
        "name"=> 'test magazine name changed',
        "api_key"=> "23233ewrwr",
        "client_id"=> $client->client_id,
        "description"=>'description changed',
        "logo"=> UploadedFile::fake()->image('image.jpg'),
      ]);
      $update->assertStatus(200);
      $update->assertJson(['message' => "Magazine Updated!"]);
      }


      public function testDeleteMagazine()
      {
        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user)->json('POST', '/api/v1/magazines');
        $response->assertStatus(200);
        $magazine = $response->getData()->result->rows[0];
        $delete = $this->actingAs($user)->json('delete', '/api/v1/magazine/'.$magazine->magazine_id);
        $delete->assertStatus(200);
        $delete->assertJson(['message' => "Magazine Deleted!"]);
     }
     public function testGettingAllMagazinesByClient()
     {
       $user = factory(\App\User::class)->create();
       $response = $this->actingAs($user)->json('POST', '/api/v1/magazines');
       $response->assertStatus(200);
       $magazine = $response->getData()->result->rows[0];
       $data['client_id']=$magazine->client_id;
       $response = $this->actingAs($user)->json('POST', '/api/v1/magazines/client',$data);
       $response->assertStatus(200);
       $response->assertJsonStructure(
           [
             'result'=>[
                         'total',
                         'rows'=>
                                   [
                                     '*'=>[
                                       "magazine_id",
                                       "client_id",
                                       "api_key",
                                       "name",
                                       "description",
                                       "logo",
                                       "deleted_at",
                                       "created_at",
                                       "updated_at"
                                       ]
                                   ]
                       ]
           ]
      );
     }
}
