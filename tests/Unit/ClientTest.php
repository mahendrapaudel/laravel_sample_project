<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClientTest extends TestCase
{

    public function testCreateClientWithMiddleware()
      {
              $data = [
                "name"=> 'test name',
                "vat_id"=> "22",
                "address_1"=> "kathmandu",
                "address_2"=>null,
                "address_3"=> null,
                "zip_code"=> 34543,
                "city"=> "ktm",
                "national_code"=> "fdsafs",
                "invoice_address_1"=> "fasdfdasf",
                "invoice_address_2"=> null,
                "invoice_address_3"=> null,
                "invoice_zipcode"=> 2222,
                "invoice_city"=> "fasdf",
                "invoice_national_code"=> "432432"
                     ];

          $response = $this->json('POST', '/api/v1/client',$data);
          $response->assertStatus(401);
          $response->assertJson(['message' => "Unauthenticated."]);
      }
      public function testCreateClient()
      {
            $data = [
              'name' => 'test name',
              'vat_id' => '22',
              'address_1' => 'kathmandu',
              'address_2' => NULL,
              'address_3' => NULL,
              'zip_code' => 34543,
              'city' => 'ktm',
              'national_code' => 'fdsafs',
              'invoice_address_1' => 'fasdfdasf',
              'invoice_address_2' => NULL,
              'invoice_address_3' => NULL,
              'invoice_zipcode' => 2222,
              'invoice_city' => 'fasdf',
              'invoice_national_code' => '432432'
                   ];
              $user = factory(\App\User::class)->create();
              $response = $this->actingAs($user)->json('POST', '/api/v1/client',$data);
              $response->assertStatus(201);
              $response->assertJson(['status' => true]);
              $response->assertJson(['message' => "Client Created!"]);
              $response->assertJson(['result' => $data]);
        }

      public function testGettingAllClients()
      {
        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user)->json('POST', '/api/v1/clients');
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
              'result'=>[
                          'total',
                          'rows'=>
                                    [
                                      '*'=>[
                                            'client_id',
                                            'name',
                                            'vat_id',
                                            'address_1',
                                            'address_2',
                                            'address_3',
                                            'zip_code',
                                            'city',
                                            'national_code',
                                            'invoice_address_1',
                                            'invoice_address_2',
                                            'invoice_address_3',
                                            'invoice_zipcode',
                                            'invoice_city',
                                            'invoice_national_code',
                                            'deleted_at',
                                            'created_at',
                                            'updated_at'
                                        ]
                                    ]
                        ]
            ]
       );
      }

      public function testUpdateClient()
      {
      $user = factory(\App\User::class)->create();
      $response = $this->actingAs($user)->json('POST', '/api/v1/clients');
      $response->assertStatus(200);
      $client = $response->getData()->result->rows[0];
      $update = $this->actingAs($user)->json('PUT', '/api/v1/client/'.$client->client_id,['name' => 'test name changed',
          'vat_id' => '22',
          'address_1' => 'kathmandu',
          'zip_code' => 34543,
          'city' => 'ktm',
          'national_code' => 'NP',
          'invoice_address_1' => 'fasdfdasf',
          'invoice_zipcode' => 2222,
          'invoice_city' => 'fasdf',
          'invoice_national_code' => '432432'
      ]);
      $update->assertStatus(200);
      $update->assertJson(['message' => "Client Updated!"]);
      }

     public function testDeleteClient()
     {
       $user = factory(\App\User::class)->create();
       $response = $this->actingAs($user)->json('POST', '/api/v1/clients');
       $response->assertStatus(200);
       $client = $response->getData()->result->rows[0];
       $delete = $this->actingAs($user)->json('delete', '/api/v1/client/'.$client->client_id);
       $delete->assertStatus(200);
       $delete->assertJson(['message' => "Client Deleted!"]);
    }

    public function testAutocompleteClient()
    {
      $user = factory(\App\User::class)->create();
      $response = $this->actingAs($user)->json('POST', '/api/v1/clients/auto-complete');
      $response->assertStatus(200);
      $response->assertJsonStructure(
          [
            'result'=>[
                        '*'=>[
                              'value',
                              'caption'
                             ]
                      ]
          ]
     );
   }
}
