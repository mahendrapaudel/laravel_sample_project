<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;

class UserTest extends TestCase
{

    public function testCreateUserWithMiddleware()
    {
      $data = [
      "name"=> 'test name',
      "email"=> "mahendra@globdig.com",
      "email_verified_at"=> "",
      "date_of_birth"=>null,
      "client_id"=> null,
      "dashboard_type"=> 1
             ];

      $response = $this->json('POST', '/api/v1/user',$data);
      $response->assertStatus(401);
      $response->assertJson(['message' => "Unauthenticated."]);
    }

    public function testCreateUser()
    {
      $user = factory(\App\User::class)->create();
      $client =factory(\App\Client::class)->create();

      $data = [
      "name"=> 'test name',
      "email"=> rand().'@test.com',
      "password"=>'1234',
      "conPassword"=>'1234',
      "email_verified_at"=> "",
      "date_of_birth"=>'1983-07-12',
      "client_id"=> $client->client_id,
      "dashboard_type"=> 1
             ];
      $response = $this->actingAs($user)->json('POST', '/api/v1/user',$data);
      $response->assertStatus(201);
      $response->assertJson(['status' => true]);
      $response->assertJson(['message' => "User Created!"]);
  }
  public function testUpdateUser()
  {
      $user = factory(\App\User::class)->create();
      $response = $this->actingAs($user)->json('POST', '/api/v1/users');
      $response->assertStatus(200);
      $user1 = $response->getData()->result->rows[0];
      $update = $this->actingAs($user)->json('PUT', '/api/v1/user/'.$user1->user_id,[
      "name"=> 'test changed user name',
      "email"=> rand().'@test.com',
      "date_of_birth"=>'1983-07-12',
      "dashboard_type"=> 1
      ]);
      $update->assertStatus(200);
      $update->assertJson(['message' => "User Updated!"]);
  }

  public function testGettingAllUsers()
  {
    $user = factory(\App\User::class)->create();
    $response = $this->actingAs($user)->json('POST', '/api/v1/users');
    $response->assertStatus(200);
    $response->assertJsonStructure(
        [
          'result'=>[
                      'total',
                      'rows'=>
                                [
                                  '*'=>[
                                    "name",
                                    "email",
                                    "email_verified_at",
                                    "date_of_birth",
                                    "client_id",
                                    "dashboard_type",
                                    "deleted_at",
                                    "created_at",
                                    "updated_at"
                                    ]
                                ]
                    ]
        ]
   );
  }

}
