<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {            
            $table->increments('client_id');           
            $table->string('name')->nullable();
            $table->string('vat_id')->nullable();
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->unsignedInteger('zip_code')->nullable();
            $table->string('city')->nullable();
            $table->string('national_code')->nullable();
            $table->string('invoice_address_1')->nullable();
            $table->string('invoice_address_2')->nullable();
            $table->string('invoice_address_3')->nullable();
            $table->unsignedInteger('invoice_zipcode')->nullable();
            $table->string('invoice_city')->nullable();
            $table->string('invoice_national_code')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
        DB::unprepared('ALTER TABLE clients SET WITH oids');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
