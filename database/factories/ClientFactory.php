<?php

use Faker\Generator as Faker;

$factory->define(App\Client::class, function (Faker $faker) {
    return [
      'name' => 'test factory name',
      'vat_id' => '22',
      'address_1' => 'kathmandu',
      'address_2' => NULL,
      'address_3' => NULL,
      'zip_code' => 34543,
      'city' => 'ktm',
      'national_code' => 'fdsafs',
      'invoice_address_1' => 'fasdfdasf',
      'invoice_address_2' => NULL,
      'invoice_address_3' => NULL,
      'invoice_zipcode' => 2222,
      'invoice_city' => 'fasdf',
      'invoice_national_code' => '432432'
    ];
});
