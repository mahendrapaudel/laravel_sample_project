---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#Clients Management

APIs for managing clients
<!-- START_ea5d45fea2067cf017070eef3c0a218a -->
## api/v1/clients
> Example request:

```bash
curl -X POST "http://localhost/api/v1/clients"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/clients");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "total": 12,
        "rows": [
            {
                "client_id": 2,
                "name": null,
                "vat_id": "22",
                "address_1": "kathmandu",
                "address_2": null,
                "address_3": null,
                "zip_code": 34543,
                "city": "ktm",
                "national_code": "fdsafs",
                "invoice_address_1": "fasdfdasf",
                "invoice_address_2": null,
                "invoice_address_3": null,
                "invoice_zipcode": 2222,
                "invoice_city": "fasdf",
                "invoice_national_code": "432432",
                "deleted_at": null,
                "created_at": "2018-12-06 15:11:42",
                "updated_at": "2018-12-06 15:11:42"
            },
            {
                "client_id": 3,
                "name": null,
                "vat_id": "22",
                "address_1": "kathmandu",
                "address_2": null,
                "address_3": null,
                "zip_code": 34543,
                "city": "ktm",
                "national_code": "fdsafs",
                "invoice_address_1": "fasdfdasf",
                "invoice_address_2": null,
                "invoice_address_3": null,
                "invoice_zipcode": 2222,
                "invoice_city": "fasdf",
                "invoice_national_code": "432432",
                "deleted_at": null,
                "created_at": "2018-12-06 15:14:23",
                "updated_at": "2018-12-06 15:14:23"
            }
        ]
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/clients`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_ea5d45fea2067cf017070eef3c0a218a -->

<!-- START_769ca62079b0ffa1a99425b5fba69d3c -->
## api/v1/client
> Example request:

```bash
curl -X POST "http://localhost/api/v1/client"     -d "name"="RgMg16yszecldb3m" \
    -d "vat_id"="nMfb2JstkQqOPQMk" \
    -d "address_1"="6JMso5h7DHKKZGiC" \
    -d "address_2"="yppVP5R9tONJbGU0" \
    -d "address_3"="rzCoZSUlTRoxxhyQ" \
    -d "zip_code"="lGVXvpf6Tb6j9r5Z" \
    -d "city"="sHH7Z6feQHtHSaIZ" \
    -d "national_code"="JgYNtaLFzVutnSD6" \
    -d "invoice_address_1"="qbqAcH3FaWjh4t7p" \
    -d "invoice_address_2"="NNvNFHUEvdIGX7LM" \
    -d "invoice_address_3"="5QWmwRwowbm6Tq8V" \
    -d "invoice_zipcode"="RlTg5e9HgV9Omf7s" \
    -d "invoice_city"="Z4E3NlNmUFw8xodB" \
    -d "invoice_national_code"="b2FEFgEOYse5kpLE" 
```

```javascript
const url = new URL("http://localhost/api/v1/client");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "RgMg16yszecldb3m",
    "vat_id": "nMfb2JstkQqOPQMk",
    "address_1": "6JMso5h7DHKKZGiC",
    "address_2": "yppVP5R9tONJbGU0",
    "address_3": "rzCoZSUlTRoxxhyQ",
    "zip_code": "lGVXvpf6Tb6j9r5Z",
    "city": "sHH7Z6feQHtHSaIZ",
    "national_code": "JgYNtaLFzVutnSD6",
    "invoice_address_1": "qbqAcH3FaWjh4t7p",
    "invoice_address_2": "NNvNFHUEvdIGX7LM",
    "invoice_address_3": "5QWmwRwowbm6Tq8V",
    "invoice_zipcode": "RlTg5e9HgV9Omf7s",
    "invoice_city": "Z4E3NlNmUFw8xodB",
    "invoice_national_code": "b2FEFgEOYse5kpLE",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "client_id": 5,
        "name": null,
        "vat_id": "d23424",
        "address_1": "Kathmandu",
        "address_2": null,
        "address_3": null,
        "zip_code": 123,
        "city": "kathmandu",
        "national_code": "no-123",
        "invoice_address_1": "Kathmandu -lalitpur",
        "invoice_address_2": null,
        "invoice_address_3": null,
        "invoice_zipcode": 123,
        "invoice_city": "Lalitpur",
        "invoice_national_code": "23233",
        "deleted_at": null,
        "created_at": "2018-12-06 15:26:30",
        "updated_at": "2018-12-06 15:26:30"
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/client`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | name.
    vat_id | string |  required  | vat_id.
    address_1 | string |  required  | address_1.
    address_2 | string |  optional  | optional address_2.
    address_3 | string |  required  | address_3.
    zip_code | string |  required  | zip_code.
    city | string |  required  | city.
    national_code | string |  required  | national_code.
    invoice_address_1 | string |  required  | invoice_address_1.
    invoice_address_2 | string |  required  | invoice_address_2.
    invoice_address_3 | string |  required  | invoice_address_3.
    invoice_zipcode | string |  required  | invoice_zipcode.
    invoice_city | string |  required  | invoice_city.
    invoice_national_code | string |  required  | invoice_national_code.

<!-- END_769ca62079b0ffa1a99425b5fba69d3c -->

<!-- START_3697c60f1e3248374ef76a6815e9dfb6 -->
## api/v1/client/{client}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/client/{client}"     -d "client_id"="16" 
```

```javascript
const url = new URL("http://localhost/api/v1/client/{client}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "client_id": "16",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "client_id": 5,
        "name": null,
        "vat_id": "d23424",
        "address_1": "Kathmandu",
        "address_2": null,
        "address_3": null,
        "zip_code": 123,
        "city": "kathmandu",
        "national_code": "no-123",
        "invoice_address_1": "Kathmandu -lalitpur",
        "invoice_address_2": null,
        "invoice_address_3": null,
        "invoice_zipcode": 123,
        "invoice_city": "Lalitpur",
        "invoice_national_code": "23233",
        "deleted_at": null,
        "created_at": "2018-12-06 15:26:30",
        "updated_at": "2018-12-06 15:26:30"
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/client/{client}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    client_id | integer |  required  | the ID of the Client

<!-- END_3697c60f1e3248374ef76a6815e9dfb6 -->

<!-- START_8b9db44c5dc7641ac8214b845ce2d048 -->
## api/v1/client/{client}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/client/{client}"     -d "name"="WT5jIOzR2UpaDhbL" \
    -d "email"="xrHmz7kQdJBlXYHr" \
    -d "date_of_birth"="27S8eLzBk1hCXQ4U" \
    -d "client_id"="4" \
    -d "tracker_type_id"="4" \
    -d "dashboard_type"="1" \
    -d "password"="avUnXurjzPh0brH7" 
```

```javascript
const url = new URL("http://localhost/api/v1/client/{client}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "WT5jIOzR2UpaDhbL",
    "email": "xrHmz7kQdJBlXYHr",
    "date_of_birth": "27S8eLzBk1hCXQ4U",
    "client_id": "4",
    "tracker_type_id": "4",
    "dashboard_type": "1",
    "password": "avUnXurjzPh0brH7",
})

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "client_id": 5,
        "name": null,
        "vat_id": "d23424",
        "address_1": "Kathmandu",
        "address_2": null,
        "address_3": null,
        "zip_code": 123,
        "city": "kathmandu",
        "national_code": "no-123",
        "invoice_address_1": "Kathmandu -lalitpur",
        "invoice_address_2": null,
        "invoice_address_3": null,
        "invoice_zipcode": 123,
        "invoice_city": "Lalitpur",
        "invoice_national_code": "23233",
        "deleted_at": null,
        "created_at": "2018-12-06 15:26:30",
        "updated_at": "2018-12-06 15:26:30"
    },
    "messages": null
}
```

### HTTP Request
`PUT api/v1/client/{client}`

`PATCH api/v1/client/{client}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | The title of the post.
    email | email |  required  | The title of the post.
    date_of_birth | date |  optional  | optional  The type of post to create. Defaults to 'textophonious'.
    client_id | integer |  required  | the ID of the Client
    tracker_type_id | integer |  required  | the ID of the Tracker Type
    dashboard_type | integer |  optional  | optional
    password | string |  required  | Password

<!-- END_8b9db44c5dc7641ac8214b845ce2d048 -->

<!-- START_caba4b46e889c55eb64444bec7bc5946 -->
## api/v1/client/{client}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/client/{client}"     -d "client_id"="7" 
```

```javascript
const url = new URL("http://localhost/api/v1/client/{client}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "client_id": "7",
})

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": "null",
    "messages": null
}
```

### HTTP Request
`DELETE api/v1/client/{client}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    client_id | integer |  required  | the ID of the Client

<!-- END_caba4b46e889c55eb64444bec7bc5946 -->

#Sources Management

APIs for managing sources
<!-- START_cbb4bdc445a1a60c4b2482a6fc3fe056 -->
## api/v1/sources
> Example request:

```bash
curl -X POST "http://localhost/api/v1/sources"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/sources");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "total": 2,
        "rows": [
            {
                "source_id": 1,
                "name": "suba sah",
                "magazine_id": 2,
                "deleted_at": null,
                "created_at": "2018-12-17 09:16:47",
                "updated_at": "2018-12-17 09:21:48"
            },
            {
                "source_id": 1,
                "name": "suba sah",
                "magazine_id": 2,
                "deleted_at": null,
                "created_at": "2018-12-17 09:16:47",
                "updated_at": "2018-12-17 09:21:48"
            }
        ]
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/sources`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_cbb4bdc445a1a60c4b2482a6fc3fe056 -->

<!-- START_2da6531e9c0c0bf60da955c7a1147bba -->
## api/v1/source
> Example request:

```bash
curl -X POST "http://localhost/api/v1/source"     -d "magazine_id"="20" \
    -d "name"="BcAbThBgNIrGXwSX" 
```

```javascript
const url = new URL("http://localhost/api/v1/source");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "magazine_id": "20",
    "name": "BcAbThBgNIrGXwSX",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "magazine_id": "2",
        "name": "suba",
        "updated_at": "2018-12-17 09:16:47",
        "created_at": "2018-12-17 09:16:47",
        "source_id": 1
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/source`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    magazine_id | integer |  required  | magazine_id, a foreign key.
    name | string |  required  | name.

<!-- END_2da6531e9c0c0bf60da955c7a1147bba -->

<!-- START_4ea499d9b1bb792f5af412ebc92d5506 -->
## api/v1/source/{source}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/source/{source}"     -d "source_id"="16" 
```

```javascript
const url = new URL("http://localhost/api/v1/source/{source}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "source_id": "16",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "magazine_id": "2",
        "name": "suba",
        "updated_at": "2018-12-17 09:16:47",
        "created_at": "2018-12-17 09:16:47",
        "source_id": 1
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/source/{source}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    source_id | integer |  required  | source_id, whose record to be seen.

<!-- END_4ea499d9b1bb792f5af412ebc92d5506 -->

<!-- START_50013f0435b47bb6a6cb90457ea5408e -->
## api/v1/source/{source}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/source/{source}"     -d "magazine_id"="2" \
    -d "name"="zilCY6PVuWJQO8hs" \
    -d "source_id"="16" 
```

```javascript
const url = new URL("http://localhost/api/v1/source/{source}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "magazine_id": "2",
    "name": "zilCY6PVuWJQO8hs",
    "source_id": "16",
})

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "magazine_id": "2",
        "name": "suba",
        "updated_at": "2018-12-17 09:16:47",
        "created_at": "2018-12-17 09:16:47",
        "source_id": 1
    },
    "messages": null
}
```

### HTTP Request
`PUT api/v1/source/{source}`

`PATCH api/v1/source/{source}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    magazine_id | integer |  required  | magazine_id, a foreign key.
    name | string |  required  | name.
    source_id | integer |  required  | source_id, whose record to be updated.

<!-- END_50013f0435b47bb6a6cb90457ea5408e -->

<!-- START_8aa4afc7ebe8dfd1b44cbe053e811a07 -->
## api/v1/source/{source}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/source/{source}"     -d "source_id"="11" 
```

```javascript
const url = new URL("http://localhost/api/v1/source/{source}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "source_id": "11",
})

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": "null",
    "messages": null
}
```

### HTTP Request
`DELETE api/v1/source/{source}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    source_id | integer |  required  | the ID of the source

<!-- END_8aa4afc7ebe8dfd1b44cbe053e811a07 -->

#Start codes Management

APIs for managing Start_codes
<!-- START_40deb53143c192469f759c06548452f1 -->
## api/v1/start-codes
> Example request:

```bash
curl -X POST "http://localhost/api/v1/start-codes"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/start-codes");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "total": 1,
        "rows": [
            {
                "start_code": 1,
                "name": "Suba Sah",
                "description": "description",
                "magazine_id": 2,
                "deleted_at": null,
                "created_at": "2018-12-17 09:37:20",
                "updated_at": "2018-12-17 09:37:20"
            }
        ]
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/start-codes`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_40deb53143c192469f759c06548452f1 -->

<!-- START_52ec3e97907a0ca3352c39e1773527dc -->
## api/v1/start-code
> Example request:

```bash
curl -X POST "http://localhost/api/v1/start-code"     -d "price"="1" \
    -d "type"="pjHjO0jNvAnbEbSW" \
    -d "magazine_id"="11" 
```

```javascript
const url = new URL("http://localhost/api/v1/start-code");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "price": "1",
    "type": "pjHjO0jNvAnbEbSW",
    "magazine_id": "11",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "price": "49058",
        "type": "234",
        "length": "23123",
        "magazine_id": "12",
        "updated_at": "2018-12-13 11:41:32",
        "created_at": "2018-12-13 11:41:32",
        "subscription_id": 6
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/start-code`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    price | integer |  required  | price.
    type | string |  required  | type.
    magazine_id | integer |  required  | magazine_id.

<!-- END_52ec3e97907a0ca3352c39e1773527dc -->

<!-- START_63bfbb0e304bb41743cf58e0aec18b73 -->
## api/v1/start-code/{start_code}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/start-code/{start_code}"     -d "Start_code_id"="2" 
```

```javascript
const url = new URL("http://localhost/api/v1/start-code/{start_code}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "Start_code_id": "2",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "subscription_id": 7,
        "price": 234324,
        "type": 234,
        "magazine_id": 12,
        "length": 23123,
        "deleted_at": null,
        "created_at": "2018-12-13 11:50:06",
        "updated_at": "2018-12-13 11:50:06"
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/start-code/{start_code}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    Start_code_id | integer |  required  | the ID of the Start_code

<!-- END_63bfbb0e304bb41743cf58e0aec18b73 -->

<!-- START_3848ca6405d833d570af8d9022cbc9a5 -->
## api/v1/start-code/{start_code}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/start-code/{start_code}"     -d "price"="6" \
    -d "type"="M7swHIRjxujl4VwG" \
    -d "magazine_id"="3" \
    -d "subscription_id"="11" 
```

```javascript
const url = new URL("http://localhost/api/v1/start-code/{start_code}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "price": "6",
    "type": "M7swHIRjxujl4VwG",
    "magazine_id": "3",
    "subscription_id": "11",
})

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "price": "49058",
        "type": "234",
        "length": "23123",
        "magazine_id": "12",
        "updated_at": "2018-12-13 11:41:32",
        "created_at": "2018-12-13 11:41:32",
        "subscription_id": 6
    },
    "messages": null
}
```

### HTTP Request
`PUT api/v1/start-code/{start_code}`

`PATCH api/v1/start-code/{start_code}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    price | integer |  required  | price.
    type | string |  required  | type.
    magazine_id | integer |  required  | magazine_id.
    subscription_id | integer |  required  | subscription_id, whose record to be updated.

<!-- END_3848ca6405d833d570af8d9022cbc9a5 -->

<!-- START_dff625b3d40dc61c00528787dd68e68f -->
## api/v1/start-code/{start_code}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/start-code/{start_code}"     -d "Start_code_id"="1" 
```

```javascript
const url = new URL("http://localhost/api/v1/start-code/{start_code}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "Start_code_id": "1",
})

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": "null",
    "messages": null
}
```

### HTTP Request
`DELETE api/v1/start-code/{start_code}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    Start_code_id | integer |  required  | the ID of the Start_code

<!-- END_dff625b3d40dc61c00528787dd68e68f -->

#Start_codes Management

APIs for managing Start_codes
<!-- START_b0ea86017c2ae3f292aa60e643e58a30 -->
## api/v1/stop-codes
> Example request:

```bash
curl -X POST "http://localhost/api/v1/stop-codes"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/stop-codes");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "total": 1,
        "rows": [
            {
                "stop_code": 1,
                "name": "Suba Sah",
                "description": "description",
                "magazine_id": 2,
                "deleted_at": null,
                "created_at": "2018-12-17 09:37:20",
                "updated_at": "2018-12-17 09:37:20"
            }
        ]
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/stop-codes`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_b0ea86017c2ae3f292aa60e643e58a30 -->

<!-- START_aa9bbd058195ef4c9b8908fdda34bba6 -->
## api/v1/stop-code
> Example request:

```bash
curl -X POST "http://localhost/api/v1/stop-code"     -d "name"="JZKIl4quPLuhCTZz" \
    -d "description"="PLkJUEtEWOzLXz7p" \
    -d "magazine_id"="12" 
```

```javascript
const url = new URL("http://localhost/api/v1/stop-code");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "JZKIl4quPLuhCTZz",
    "description": "PLkJUEtEWOzLXz7p",
    "magazine_id": "12",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "name": "Suba Sah",
        "description": "description",
        "magazine_id": "2",
        "updated_at": "2018-12-17 09:37:20",
        "created_at": "2018-12-17 09:37:20",
        "stop_code": 1
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/stop-code`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | name.
    description | string |  required  | description.
    magazine_id | integer |  required  | magazine_id, a foreign key.

<!-- END_aa9bbd058195ef4c9b8908fdda34bba6 -->

<!-- START_7297e78ac4e897e0567d14890534f7b6 -->
## api/v1/stop-code/{stop_code}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/stop-code/{stop_code}"     -d "name"="Y5ZBqJUXZAMk3ylS" \
    -d "description"="dLACwHDHtVUdIMiY" \
    -d "magazine_id"="2" \
    -d "stop_code_id"="14" 
```

```javascript
const url = new URL("http://localhost/api/v1/stop-code/{stop_code}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "Y5ZBqJUXZAMk3ylS",
    "description": "dLACwHDHtVUdIMiY",
    "magazine_id": "2",
    "stop_code_id": "14",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "name": "Suba Sah",
        "description": "description",
        "magazine_id": "2",
        "updated_at": "2018-12-17 09:37:20",
        "created_at": "2018-12-17 09:37:20",
        "stop_code": 1
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/stop-code/{stop_code}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | name.
    description | string |  required  | description.
    magazine_id | integer |  required  | magazine_id, a foreign key.
    stop_code_id | integer |  required  | stop_code_id, whose record to be seen.

<!-- END_7297e78ac4e897e0567d14890534f7b6 -->

<!-- START_43d47fb66b64c758c58c69489f48209f -->
## api/v1/stop-code/{stop_code}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/stop-code/{stop_code}"     -d "name"="7W4oLjuuNVNzLWkr" \
    -d "description"="WMfBU0SqAVm0pXFo" \
    -d "magazine_id"="17" 
```

```javascript
const url = new URL("http://localhost/api/v1/stop-code/{stop_code}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "7W4oLjuuNVNzLWkr",
    "description": "WMfBU0SqAVm0pXFo",
    "magazine_id": "17",
})

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "name": "Suba Sah",
        "description": "description",
        "magazine_id": "2",
        "updated_at": "2018-12-17 09:37:20",
        "created_at": "2018-12-17 09:37:20",
        "stop_code": 1
    },
    "messages": null
}
```

### HTTP Request
`PUT api/v1/stop-code/{stop_code}`

`PATCH api/v1/stop-code/{stop_code}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | name.
    description | string |  required  | description.
    magazine_id | integer |  required  | magazine_id, a foreign key.

<!-- END_43d47fb66b64c758c58c69489f48209f -->

<!-- START_5e266026a353b10ca5b97fd897575ca4 -->
## api/v1/stop-code/{stop_code}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/stop-code/{stop_code}"     -d "stop_code_id"="11" 
```

```javascript
const url = new URL("http://localhost/api/v1/stop-code/{stop_code}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "stop_code_id": "11",
})

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": "null",
    "messages": null
}
```

### HTTP Request
`DELETE api/v1/stop-code/{stop_code}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    stop_code_id | integer |  required  | the ID of the Stop_code

<!-- END_5e266026a353b10ca5b97fd897575ca4 -->

#Tracker management

APIs for managing users
<!-- START_f3ac5b3d8ab781ebc912fb259bec57f6 -->
## api/v1/trackers
> Example request:

```bash
curl -X POST "http://localhost/api/v1/trackers"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/trackers");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "total": 12,
        "rows": [
            {
                "client_id": 2,
                "name": null,
                "vat_id": "22",
                "address_1": "kathmandu",
                "address_2": null,
                "address_3": null,
                "zip_code": 34543,
                "city": "ktm",
                "national_code": "fdsafs",
                "invoice_address_1": "fasdfdasf",
                "invoice_address_2": null,
                "invoice_address_3": null,
                "invoice_zipcode": 2222,
                "invoice_city": "fasdf",
                "invoice_national_code": "432432",
                "deleted_at": null,
                "created_at": "2018-12-06 15:11:42",
                "updated_at": "2018-12-06 15:11:42"
            },
            {
                "client_id": 3,
                "name": null,
                "vat_id": "22",
                "address_1": "kathmandu",
                "address_2": null,
                "address_3": null,
                "zip_code": 34543,
                "city": "ktm",
                "national_code": "fdsafs",
                "invoice_address_1": "fasdfdasf",
                "invoice_address_2": null,
                "invoice_address_3": null,
                "invoice_zipcode": 2222,
                "invoice_city": "fasdf",
                "invoice_national_code": "432432",
                "deleted_at": null,
                "created_at": "2018-12-06 15:14:23",
                "updated_at": "2018-12-06 15:14:23"
            }
        ]
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/trackers`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_f3ac5b3d8ab781ebc912fb259bec57f6 -->

<!-- START_2c1069731c7cd131466baa9dad753da7 -->
## api/v1/tracker
> Example request:

```bash
curl -X POST "http://localhost/api/v1/tracker"     -d "name"="GD6ZAOUuSL9sDqPz" \
    -d "vat_id"="ivJbMEdU22pxxYGG" \
    -d "address_1"="itR7v3ohJv3f1OBk" \
    -d "address_2"="Kvzju2Bccwu0Vbl7" \
    -d "address_3"="9cGdihy3BStOGwQK" \
    -d "zip_code"="8DWybvXbQ2mxA6zF" \
    -d "city"="mq8KdY12u1doZUvy" \
    -d "national_code"="M3JAohO5LCYZBpw1" \
    -d "invoice_address_1"="pdzBWeXxjlVeY19M" \
    -d "invoice_address_2"="0b8DG4hPZgMSc8qW" \
    -d "invoice_address_3"="uwV1MyHPMlsfceEV" \
    -d "invoice_zipcode"="2zjBGZ0Dc3vvotBZ" \
    -d "invoice_city"="iavxIxBQFQ2tF9Ik" \
    -d "invoice_national_code"="lSwcPLkwcp5aYA5n" 
```

```javascript
const url = new URL("http://localhost/api/v1/tracker");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "GD6ZAOUuSL9sDqPz",
    "vat_id": "ivJbMEdU22pxxYGG",
    "address_1": "itR7v3ohJv3f1OBk",
    "address_2": "Kvzju2Bccwu0Vbl7",
    "address_3": "9cGdihy3BStOGwQK",
    "zip_code": "8DWybvXbQ2mxA6zF",
    "city": "mq8KdY12u1doZUvy",
    "national_code": "M3JAohO5LCYZBpw1",
    "invoice_address_1": "pdzBWeXxjlVeY19M",
    "invoice_address_2": "0b8DG4hPZgMSc8qW",
    "invoice_address_3": "uwV1MyHPMlsfceEV",
    "invoice_zipcode": "2zjBGZ0Dc3vvotBZ",
    "invoice_city": "iavxIxBQFQ2tF9Ik",
    "invoice_national_code": "lSwcPLkwcp5aYA5n",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "client_id": 5,
        "name": null,
        "vat_id": "d23424",
        "address_1": "Kathmandu",
        "address_2": null,
        "address_3": null,
        "zip_code": 123,
        "city": "kathmandu",
        "national_code": "no-123",
        "invoice_address_1": "Kathmandu -lalitpur",
        "invoice_address_2": null,
        "invoice_address_3": null,
        "invoice_zipcode": 123,
        "invoice_city": "Lalitpur",
        "invoice_national_code": "23233",
        "deleted_at": null,
        "created_at": "2018-12-06 15:26:30",
        "updated_at": "2018-12-06 15:26:30"
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/tracker`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | name.
    vat_id | string |  required  | vat_id.
    address_1 | string |  required  | address_1.
    address_2 | string |  optional  | optional address_2.
    address_3 | string |  required  | address_3.
    zip_code | string |  required  | zip_code.
    city | string |  required  | city.
    national_code | string |  required  | national_code.
    invoice_address_1 | string |  required  | invoice_address_1.
    invoice_address_2 | string |  required  | invoice_address_2.
    invoice_address_3 | string |  required  | invoice_address_3.
    invoice_zipcode | string |  required  | invoice_zipcode.
    invoice_city | string |  required  | invoice_city.
    invoice_national_code | string |  required  | invoice_national_code.

<!-- END_2c1069731c7cd131466baa9dad753da7 -->

<!-- START_c6c4acbbf9e0519b94a7899e93a3031e -->
## api/v1/tracker/{tracker}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/tracker/{tracker}"     -d "client_id"="10" 
```

```javascript
const url = new URL("http://localhost/api/v1/tracker/{tracker}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "client_id": "10",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "client_id": 5,
        "name": null,
        "vat_id": "d23424",
        "address_1": "Kathmandu",
        "address_2": null,
        "address_3": null,
        "zip_code": 123,
        "city": "kathmandu",
        "national_code": "no-123",
        "invoice_address_1": "Kathmandu -lalitpur",
        "invoice_address_2": null,
        "invoice_address_3": null,
        "invoice_zipcode": 123,
        "invoice_city": "Lalitpur",
        "invoice_national_code": "23233",
        "deleted_at": null,
        "created_at": "2018-12-06 15:26:30",
        "updated_at": "2018-12-06 15:26:30"
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/tracker/{tracker}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    client_id | integer |  required  | the ID of the Tracker

<!-- END_c6c4acbbf9e0519b94a7899e93a3031e -->

<!-- START_66490859107c132a36f96c38cca8edcd -->
## api/v1/tracker/{tracker}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/tracker/{tracker}"     -d "name"="AeEpyp8sibROOVLY" \
    -d "email"="whhCrPD6PSE56B2e" \
    -d "date_of_birth"="4fbcsG7O7EUCIdhF" \
    -d "client_id"="5" \
    -d "tracker_type_id"="7" \
    -d "dashboard_type"="11" \
    -d "password"="ZGeKjSiLyoodXtLJ" 
```

```javascript
const url = new URL("http://localhost/api/v1/tracker/{tracker}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "AeEpyp8sibROOVLY",
    "email": "whhCrPD6PSE56B2e",
    "date_of_birth": "4fbcsG7O7EUCIdhF",
    "client_id": "5",
    "tracker_type_id": "7",
    "dashboard_type": "11",
    "password": "ZGeKjSiLyoodXtLJ",
})

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "client_id": 5,
        "name": null,
        "vat_id": "d23424",
        "address_1": "Kathmandu",
        "address_2": null,
        "address_3": null,
        "zip_code": 123,
        "city": "kathmandu",
        "national_code": "no-123",
        "invoice_address_1": "Kathmandu -lalitpur",
        "invoice_address_2": null,
        "invoice_address_3": null,
        "invoice_zipcode": 123,
        "invoice_city": "Lalitpur",
        "invoice_national_code": "23233",
        "deleted_at": null,
        "created_at": "2018-12-06 15:26:30",
        "updated_at": "2018-12-06 15:26:30"
    },
    "messages": null
}
```

### HTTP Request
`PUT api/v1/tracker/{tracker}`

`PATCH api/v1/tracker/{tracker}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | The title of the post.
    email | email |  required  | The title of the post.
    date_of_birth | date |  optional  | optional  The type of post to create. Defaults to 'textophonious'.
    client_id | integer |  required  | the ID of the Tracker
    tracker_type_id | integer |  required  | the ID of the Tracker Type
    dashboard_type | integer |  optional  | optional
    password | string |  required  | Password

<!-- END_66490859107c132a36f96c38cca8edcd -->

<!-- START_7282c7370d1630ad3415b9587b72a46d -->
## api/v1/tracker/{tracker}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/tracker/{tracker}"     -d "client_id"="16" 
```

```javascript
const url = new URL("http://localhost/api/v1/tracker/{tracker}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "client_id": "16",
})

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": "null",
    "messages": null
}
```

### HTTP Request
`DELETE api/v1/tracker/{tracker}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    client_id | integer |  required  | the ID of the Tracker

<!-- END_7282c7370d1630ad3415b9587b72a46d -->

#Tracker_contacts Management

APIs for managing Tracker_contacts
<!-- START_d5e3cc9345f50d3da5d512b3225c282d -->
## api/v1/tracker-contacts
> Example request:

```bash
curl -X POST "http://localhost/api/v1/tracker-contacts"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/tracker-contacts");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "total": 1,
        "rows": [
            {
                "tracker_contact_id": 1,
                "tracker_id": 2,
                "contact_type_id": 1,
                "contact": "werwerwer",
                "deleted_at": null,
                "created_at": "2018-12-17 11:35:39",
                "updated_at": "2018-12-17 11:38:57"
            }
        ]
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/tracker-contacts`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_d5e3cc9345f50d3da5d512b3225c282d -->

<!-- START_ec3dedb7a5c3c6a2c67b0be5e2295cb9 -->
## api/v1/tracker-contact
> Example request:

```bash
curl -X POST "http://localhost/api/v1/tracker-contact"     -d "tracker_id"="1" \
    -d "contact_type_id"="12" \
    -d "contact"="8yIPELuvnfwu0nK2" 
```

```javascript
const url = new URL("http://localhost/api/v1/tracker-contact");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "tracker_id": "1",
    "contact_type_id": "12",
    "contact": "8yIPELuvnfwu0nK2",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "tracker_id": "2",
        "contact_type_id": "1",
        "contact": "asdasd",
        "updated_at": "2018-12-17 11:35:39",
        "created_at": "2018-12-17 11:35:39",
        "tracker_contact_id": 1
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/tracker-contact`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    tracker_id | integer |  required  | tracker_id, a foreign key.
    contact_type_id | integer |  required  | contact_type_id, a foreign key.
    contact | string |  required  | contact.

<!-- END_ec3dedb7a5c3c6a2c67b0be5e2295cb9 -->

<!-- START_7ab4fbdee86bf2d4f8837f6836d40e8d -->
## api/v1/tracker-contact/{tracker_contact}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/tracker-contact/{tracker_contact}"     -d "tracker_id"="14" \
    -d "contact_type_id"="14" \
    -d "contact"="EXBDLCzrB3LrL0VN" \
    -d "tracker_contact_id"="18" 
```

```javascript
const url = new URL("http://localhost/api/v1/tracker-contact/{tracker_contact}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "tracker_id": "14",
    "contact_type_id": "14",
    "contact": "EXBDLCzrB3LrL0VN",
    "tracker_contact_id": "18",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "tracker_id": "2",
        "contact_type_id": "1",
        "contact": "asdasd",
        "updated_at": "2018-12-17 11:35:39",
        "created_at": "2018-12-17 11:35:39",
        "tracker_contact_id": 1
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/tracker-contact/{tracker_contact}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    tracker_id | integer |  required  | tracker_id, a foreign key.
    contact_type_id | integer |  required  | contact_type_id, a foreign key.
    contact | string |  required  | contact.
    tracker_contact_id | integer |  required  | tracker_contact_id, whose record to be seen.

<!-- END_7ab4fbdee86bf2d4f8837f6836d40e8d -->

<!-- START_30f45566b42c3697c148f2290187febf -->
## api/v1/tracker-contact/{tracker_contact}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/tracker-contact/{tracker_contact}"     -d "tracker_id"="11" \
    -d "contact_type_id"="15" \
    -d "contact"="1KHPjBCZa3yF64FF" \
    -d "tracker_contact_id"="13" 
```

```javascript
const url = new URL("http://localhost/api/v1/tracker-contact/{tracker_contact}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "tracker_id": "11",
    "contact_type_id": "15",
    "contact": "1KHPjBCZa3yF64FF",
    "tracker_contact_id": "13",
})

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "tracker_id": "2",
        "contact_type_id": "1",
        "contact": "asdasd",
        "updated_at": "2018-12-17 11:35:39",
        "created_at": "2018-12-17 11:35:39",
        "tracker_contact_id": 1
    },
    "messages": null
}
```

### HTTP Request
`PUT api/v1/tracker-contact/{tracker_contact}`

`PATCH api/v1/tracker-contact/{tracker_contact}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    tracker_id | integer |  required  | tracker_id, a foreign key.
    contact_type_id | integer |  required  | contact_type_id, a foreign key.
    contact | string |  required  | contact.
    tracker_contact_id | integer |  required  | tracker_contact_id, whose record to be updated.

<!-- END_30f45566b42c3697c148f2290187febf -->

<!-- START_72180fc01c8e12e2eec985ac4207a00f -->
## api/v1/tracker-contact/{tracker_contact}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/tracker-contact/{tracker_contact}"     -d "Tracker_contact_id"="13" 
```

```javascript
const url = new URL("http://localhost/api/v1/tracker-contact/{tracker_contact}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "Tracker_contact_id": "13",
})

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": "null",
    "messages": null
}
```

### HTTP Request
`DELETE api/v1/tracker-contact/{tracker_contact}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    Tracker_contact_id | integer |  required  | the ID of the Tracker_contact

<!-- END_72180fc01c8e12e2eec985ac4207a00f -->

#Tracker_types Management

APIs for managing Tracker_types
<!-- START_c319a30c1ca93eee4fdecf2e3229015b -->
## api/v1/tracker-types
> Example request:

```bash
curl -X POST "http://localhost/api/v1/tracker-types"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/tracker-types");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "total": 1,
        "rows": [
            {
                "tracker_type_id": 1,
                "name": "asdasd",
                "network_admin": 34234,
                "deleted_at": null,
                "created_at": "2018-12-17 11:51:04",
                "updated_at": "2018-12-17 11:51:04"
            }
        ]
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/tracker-types`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_c319a30c1ca93eee4fdecf2e3229015b -->

<!-- START_b3bc1cb251e0e239e15c4c7aee449acc -->
## api/v1/tracker-type
> Example request:

```bash
curl -X POST "http://localhost/api/v1/tracker-type"     -d "name"="3g93R5D9M9AlOvwK" \
    -d "network_admin"="13" 
```

```javascript
const url = new URL("http://localhost/api/v1/tracker-type");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "3g93R5D9M9AlOvwK",
    "network_admin": "13",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "name": "asdasd",
        "network_admin": "34234",
        "updated_at": "2018-12-17 11:51:04",
        "created_at": "2018-12-17 11:51:04",
        "tracker_type_id": 1
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/tracker-type`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | name.
    network_admin | integer |  required  | network_admin.

<!-- END_b3bc1cb251e0e239e15c4c7aee449acc -->

<!-- START_4d01108907fe0e0581eca5f44dbf236d -->
## api/v1/tracker-type/{tracker_type}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/tracker-type/{tracker_type}"     -d "name"="aoRdHl7TblLSLvai" \
    -d "network_admin"="20" \
    -d "tracker_type_id"="12" 
```

```javascript
const url = new URL("http://localhost/api/v1/tracker-type/{tracker_type}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "aoRdHl7TblLSLvai",
    "network_admin": "20",
    "tracker_type_id": "12",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "name": "asdasd",
        "network_admin": "34234",
        "updated_at": "2018-12-17 11:51:04",
        "created_at": "2018-12-17 11:51:04",
        "tracker_type_id": 1
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/tracker-type/{tracker_type}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | name.
    network_admin | integer |  required  | network_admin.
    tracker_type_id | integer |  required  | tracker_type_id, whose record to be seen.

<!-- END_4d01108907fe0e0581eca5f44dbf236d -->

<!-- START_8abbe851d3ca6a9f8f0e858a4d633963 -->
## api/v1/tracker-type/{tracker_type}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/tracker-type/{tracker_type}"     -d "name"="IBQ2iQN9MNW51Fhv" \
    -d "network_admin"="16" \
    -d "tracker_type_id"="12" 
```

```javascript
const url = new URL("http://localhost/api/v1/tracker-type/{tracker_type}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "IBQ2iQN9MNW51Fhv",
    "network_admin": "16",
    "tracker_type_id": "12",
})

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "name": "asdasd",
        "network_admin": "34234",
        "updated_at": "2018-12-17 11:51:04",
        "created_at": "2018-12-17 11:51:04",
        "tracker_type_id": 1
    },
    "messages": null
}
```

### HTTP Request
`PUT api/v1/tracker-type/{tracker_type}`

`PATCH api/v1/tracker-type/{tracker_type}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | name.
    network_admin | integer |  required  | network_admin.
    tracker_type_id | integer |  required  | tracker_type_id, whose record to be seen.

<!-- END_8abbe851d3ca6a9f8f0e858a4d633963 -->

<!-- START_16718f8a327dbddc1a2322ce1981e21b -->
## api/v1/tracker-type/{tracker_type}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/tracker-type/{tracker_type}"     -d "Tracker_type_id"="16" 
```

```javascript
const url = new URL("http://localhost/api/v1/tracker-type/{tracker_type}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "Tracker_type_id": "16",
})

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": "null",
    "messages": null
}
```

### HTTP Request
`DELETE api/v1/tracker-type/{tracker_type}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    Tracker_type_id | integer |  required  | the ID of the Tracker_type

<!-- END_16718f8a327dbddc1a2322ce1981e21b -->

#User management

APIs for managing users
<!-- START_4194ceb9a20b7f80b61d14d44df366b4 -->
## api/v1/users
> Example request:

```bash
curl -X POST "http://localhost/api/v1/users"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/users");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "total": 12,
        "rows": [
            {
                "client_id": 2,
                "name": null,
                "vat_id": "22",
                "address_1": "kathmandu",
                "address_2": null,
                "address_3": null,
                "zip_code": 34543,
                "city": "ktm",
                "national_code": "fdsafs",
                "invoice_address_1": "fasdfdasf",
                "invoice_address_2": null,
                "invoice_address_3": null,
                "invoice_zipcode": 2222,
                "invoice_city": "fasdf",
                "invoice_national_code": "432432",
                "deleted_at": null,
                "created_at": "2018-12-06 15:11:42",
                "updated_at": "2018-12-06 15:11:42"
            },
            {
                "client_id": 3,
                "name": null,
                "vat_id": "22",
                "address_1": "kathmandu",
                "address_2": null,
                "address_3": null,
                "zip_code": 34543,
                "city": "ktm",
                "national_code": "fdsafs",
                "invoice_address_1": "fasdfdasf",
                "invoice_address_2": null,
                "invoice_address_3": null,
                "invoice_zipcode": 2222,
                "invoice_city": "fasdf",
                "invoice_national_code": "432432",
                "deleted_at": null,
                "created_at": "2018-12-06 15:14:23",
                "updated_at": "2018-12-06 15:14:23"
            }
        ]
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/users`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_4194ceb9a20b7f80b61d14d44df366b4 -->

<!-- START_96b8840d06e94c53a87e83e9edfb44eb -->
## api/v1/user
> Example request:

```bash
curl -X POST "http://localhost/api/v1/user"     -d "name"="9AYUl1g29Bzshqq0" \
    -d "vat_id"="v5bzbhEktlV6uaOd" \
    -d "address_1"="dOLEiNByChqGYJfX" \
    -d "address_2"="nzljp1QEw3KBXqxG" \
    -d "address_3"="iK63gEFHMf5IruA7" \
    -d "zip_code"="dYpF7XK270gPAl13" \
    -d "city"="GcNGwdkWQZQ18Kin" \
    -d "national_code"="sDkEa4DrPKex91bo" \
    -d "invoice_address_1"="iR4AmM4d8vQD8U9I" \
    -d "invoice_address_2"="hhncWvD4lT9sIkOR" \
    -d "invoice_address_3"="geT3Z8yJGDDf92GP" \
    -d "invoice_zipcode"="gucp9Z2dGtIwaeZ3" \
    -d "invoice_city"="iIbI7586FQL1aCcY" \
    -d "invoice_national_code"="4u7zRI90AjTzmdJB" 
```

```javascript
const url = new URL("http://localhost/api/v1/user");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "9AYUl1g29Bzshqq0",
    "vat_id": "v5bzbhEktlV6uaOd",
    "address_1": "dOLEiNByChqGYJfX",
    "address_2": "nzljp1QEw3KBXqxG",
    "address_3": "iK63gEFHMf5IruA7",
    "zip_code": "dYpF7XK270gPAl13",
    "city": "GcNGwdkWQZQ18Kin",
    "national_code": "sDkEa4DrPKex91bo",
    "invoice_address_1": "iR4AmM4d8vQD8U9I",
    "invoice_address_2": "hhncWvD4lT9sIkOR",
    "invoice_address_3": "geT3Z8yJGDDf92GP",
    "invoice_zipcode": "gucp9Z2dGtIwaeZ3",
    "invoice_city": "iIbI7586FQL1aCcY",
    "invoice_national_code": "4u7zRI90AjTzmdJB",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "client_id": 5,
        "name": null,
        "vat_id": "d23424",
        "address_1": "Kathmandu",
        "address_2": null,
        "address_3": null,
        "zip_code": 123,
        "city": "kathmandu",
        "national_code": "no-123",
        "invoice_address_1": "Kathmandu -lalitpur",
        "invoice_address_2": null,
        "invoice_address_3": null,
        "invoice_zipcode": 123,
        "invoice_city": "Lalitpur",
        "invoice_national_code": "23233",
        "deleted_at": null,
        "created_at": "2018-12-06 15:26:30",
        "updated_at": "2018-12-06 15:26:30"
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/user`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | name.
    vat_id | string |  required  | vat_id.
    address_1 | string |  required  | address_1.
    address_2 | string |  optional  | optional address_2.
    address_3 | string |  required  | address_3.
    zip_code | string |  required  | zip_code.
    city | string |  required  | city.
    national_code | string |  required  | national_code.
    invoice_address_1 | string |  required  | invoice_address_1.
    invoice_address_2 | string |  required  | invoice_address_2.
    invoice_address_3 | string |  required  | invoice_address_3.
    invoice_zipcode | string |  required  | invoice_zipcode.
    invoice_city | string |  required  | invoice_city.
    invoice_national_code | string |  required  | invoice_national_code.

<!-- END_96b8840d06e94c53a87e83e9edfb44eb -->

<!-- START_a8f148df1f2cd4bc2d67314d2cb9fa3d -->
## api/v1/user/{user}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/user/{user}"     -d "client_id"="7" 
```

```javascript
const url = new URL("http://localhost/api/v1/user/{user}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "client_id": "7",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "client_id": 5,
        "name": null,
        "vat_id": "d23424",
        "address_1": "Kathmandu",
        "address_2": null,
        "address_3": null,
        "zip_code": 123,
        "city": "kathmandu",
        "national_code": "no-123",
        "invoice_address_1": "Kathmandu -lalitpur",
        "invoice_address_2": null,
        "invoice_address_3": null,
        "invoice_zipcode": 123,
        "invoice_city": "Lalitpur",
        "invoice_national_code": "23233",
        "deleted_at": null,
        "created_at": "2018-12-06 15:26:30",
        "updated_at": "2018-12-06 15:26:30"
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/user/{user}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    client_id | integer |  required  | the ID of the User

<!-- END_a8f148df1f2cd4bc2d67314d2cb9fa3d -->

<!-- START_1006d782d67bb58039bde349972eb2f0 -->
## api/v1/user/{user}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/user/{user}"     -d "name"="WqaeQwmp6mWitIHH" \
    -d "email"="GTJS6wFNhTJZARVY" \
    -d "date_of_birth"="9peKijwA8hdeQqVn" \
    -d "client_id"="10" \
    -d "tracker_type_id"="10" \
    -d "dashboard_type"="18" \
    -d "password"="H2Reii0ocgrkpNyg" 
```

```javascript
const url = new URL("http://localhost/api/v1/user/{user}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "WqaeQwmp6mWitIHH",
    "email": "GTJS6wFNhTJZARVY",
    "date_of_birth": "9peKijwA8hdeQqVn",
    "client_id": "10",
    "tracker_type_id": "10",
    "dashboard_type": "18",
    "password": "H2Reii0ocgrkpNyg",
})

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "client_id": 5,
        "name": null,
        "vat_id": "d23424",
        "address_1": "Kathmandu",
        "address_2": null,
        "address_3": null,
        "zip_code": 123,
        "city": "kathmandu",
        "national_code": "no-123",
        "invoice_address_1": "Kathmandu -lalitpur",
        "invoice_address_2": null,
        "invoice_address_3": null,
        "invoice_zipcode": 123,
        "invoice_city": "Lalitpur",
        "invoice_national_code": "23233",
        "deleted_at": null,
        "created_at": "2018-12-06 15:26:30",
        "updated_at": "2018-12-06 15:26:30"
    },
    "messages": null
}
```

### HTTP Request
`PUT api/v1/user/{user}`

`PATCH api/v1/user/{user}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | The title of the post.
    email | email |  required  | The title of the post.
    date_of_birth | date |  optional  | optional  The type of post to create. Defaults to 'textophonious'.
    client_id | integer |  required  | the ID of the User
    tracker_type_id | integer |  required  | the ID of the Tracker Type
    dashboard_type | integer |  optional  | optional
    password | string |  required  | Password

<!-- END_1006d782d67bb58039bde349972eb2f0 -->

<!-- START_a5d7655acadc1b6c97d48e68f1e87be9 -->
## api/v1/user/{user}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/user/{user}"     -d "client_id"="20" 
```

```javascript
const url = new URL("http://localhost/api/v1/user/{user}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "client_id": "20",
})

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": "null",
    "messages": null
}
```

### HTTP Request
`DELETE api/v1/user/{user}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    client_id | integer |  required  | the ID of the User

<!-- END_a5d7655acadc1b6c97d48e68f1e87be9 -->

<!-- START_d7f5c16f3f30bc08c462dbfe4b62c6b9 -->
## api/v1/user
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/user"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/user");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "total": 12,
        "rows": [
            {
                "client_id": 2,
                "name": null,
                "vat_id": "22",
                "address_1": "kathmandu",
                "address_2": null,
                "address_3": null,
                "zip_code": 34543,
                "city": "ktm",
                "national_code": "fdsafs",
                "invoice_address_1": "fasdfdasf",
                "invoice_address_2": null,
                "invoice_address_3": null,
                "invoice_zipcode": 2222,
                "invoice_city": "fasdf",
                "invoice_national_code": "432432",
                "deleted_at": null,
                "created_at": "2018-12-06 15:11:42",
                "updated_at": "2018-12-06 15:11:42"
            },
            {
                "client_id": 3,
                "name": null,
                "vat_id": "22",
                "address_1": "kathmandu",
                "address_2": null,
                "address_3": null,
                "zip_code": 34543,
                "city": "ktm",
                "national_code": "fdsafs",
                "invoice_address_1": "fasdfdasf",
                "invoice_address_2": null,
                "invoice_address_3": null,
                "invoice_zipcode": 2222,
                "invoice_city": "fasdf",
                "invoice_national_code": "432432",
                "deleted_at": null,
                "created_at": "2018-12-06 15:14:23",
                "updated_at": "2018-12-06 15:14:23"
            }
        ]
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/user`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_d7f5c16f3f30bc08c462dbfe4b62c6b9 -->

#campaigns Management

APIs for managing campaigns
<!-- START_0326bdcf6210cb7587c73a4fb46056d9 -->
## api/v1/campaigns
> Example request:

```bash
curl -X POST "http://localhost/api/v1/campaigns"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/campaigns");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "total": 2,
        "rows": [
            {
                "subscription_id": 2,
                "price": 234234,
                "type": 45,
                "magazine_id": null,
                "length": 345345,
                "deleted_at": null,
                "created_at": "2018-12-13 09:59:50",
                "updated_at": "2018-12-13 10:00:34"
            },
            {
                "subscription_id": 3,
                "price": 49058,
                "type": 234,
                "magazine_id": null,
                "length": 23123,
                "deleted_at": null,
                "created_at": "2018-12-13 10:07:42",
                "updated_at": "2018-12-13 10:07:42"
            }
        ]
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/campaigns`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_0326bdcf6210cb7587c73a4fb46056d9 -->

<!-- START_c6e238630d13a3979783364e09e71e5d -->
## api/v1/campaign
> Example request:

```bash
curl -X POST "http://localhost/api/v1/campaign"     -d "price"="11" \
    -d "type"="huyk0UXcNhvRTtt0" \
    -d "magazine_id"="10" 
```

```javascript
const url = new URL("http://localhost/api/v1/campaign");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "price": "11",
    "type": "huyk0UXcNhvRTtt0",
    "magazine_id": "10",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "price": "49058",
        "type": "234",
        "length": "23123",
        "magazine_id": "12",
        "updated_at": "2018-12-13 11:41:32",
        "created_at": "2018-12-13 11:41:32",
        "subscription_id": 6
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/campaign`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    price | integer |  required  | price.
    type | string |  required  | type.
    magazine_id | integer |  required  | magazine_id.

<!-- END_c6e238630d13a3979783364e09e71e5d -->

<!-- START_7169f2c20fb066f57f42e6e86650ea7d -->
## api/v1/campaign/{campaign}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/campaign/{campaign}"     -d "campaign_id"="20" 
```

```javascript
const url = new URL("http://localhost/api/v1/campaign/{campaign}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "campaign_id": "20",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "subscription_id": 7,
        "price": 234324,
        "type": 234,
        "magazine_id": 12,
        "length": 23123,
        "deleted_at": null,
        "created_at": "2018-12-13 11:50:06",
        "updated_at": "2018-12-13 11:50:06"
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/campaign/{campaign}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    campaign_id | integer |  required  | the ID of the campaign

<!-- END_7169f2c20fb066f57f42e6e86650ea7d -->

<!-- START_75feba3ef3e44a6049e81076b87d7c6b -->
## api/v1/campaign/{campaign}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/campaign/{campaign}"     -d "price"="5" \
    -d "type"="w07vyhUSUvbnmegv" \
    -d "magazine_id"="18" \
    -d "subscription_id"="11" 
```

```javascript
const url = new URL("http://localhost/api/v1/campaign/{campaign}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "price": "5",
    "type": "w07vyhUSUvbnmegv",
    "magazine_id": "18",
    "subscription_id": "11",
})

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "price": "49058",
        "type": "234",
        "length": "23123",
        "magazine_id": "12",
        "updated_at": "2018-12-13 11:41:32",
        "created_at": "2018-12-13 11:41:32",
        "subscription_id": 6
    },
    "messages": null
}
```

### HTTP Request
`PUT api/v1/campaign/{campaign}`

`PATCH api/v1/campaign/{campaign}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    price | integer |  required  | price.
    type | string |  required  | type.
    magazine_id | integer |  required  | magazine_id.
    subscription_id | integer |  required  | subscription_id, whose record to be updated.

<!-- END_75feba3ef3e44a6049e81076b87d7c6b -->

<!-- START_28debca6fb19ea6d8f5055252d824213 -->
## api/v1/campaign/{campaign}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/campaign/{campaign}"     -d "campaign_id"="8" 
```

```javascript
const url = new URL("http://localhost/api/v1/campaign/{campaign}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "campaign_id": "8",
})

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": "null",
    "messages": null
}
```

### HTTP Request
`DELETE api/v1/campaign/{campaign}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    campaign_id | integer |  required  | the ID of the campaign

<!-- END_28debca6fb19ea6d8f5055252d824213 -->

#categorys Management

APIs for managing categorys
<!-- START_51652a01dd7666395568dd6ba9d67d58 -->
## api/v1/categories
> Example request:

```bash
curl -X POST "http://localhost/api/v1/categories"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/categories");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "total": 1,
        "rows": [
            {
                "category_id": 1,
                "name": "asdasd",
                "description": "asdasd",
                "deleted_at": null,
                "created_at": "2018-12-13 10:06:17",
                "updated_at": "2018-12-13 10:06:44"
            }
        ]
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/categories`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_51652a01dd7666395568dd6ba9d67d58 -->

<!-- START_c5ef86a407a7b87c648bcaa4591381fd -->
## api/v1/category
> Example request:

```bash
curl -X POST "http://localhost/api/v1/category"     -d "name"="0J1URhPpS1hOhECG" \
    -d "description"="oM3339obk3cQgMZn" 
```

```javascript
const url = new URL("http://localhost/api/v1/category");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "0J1URhPpS1hOhECG",
    "description": "oM3339obk3cQgMZn",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "name": "seoiru",
        "description": "asadasd",
        "updated_at": "2018-12-13 11:57:59",
        "created_at": "2018-12-13 11:57:59",
        "category_id": 2
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/category`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | name.
    description | string |  required  | Description.

<!-- END_c5ef86a407a7b87c648bcaa4591381fd -->

<!-- START_3472a3f42a5b1dc76bb66947128507de -->
## api/v1/category/{category}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/category/{category}"     -d "category_id"="11" 
```

```javascript
const url = new URL("http://localhost/api/v1/category/{category}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "category_id": "11",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "category_id": 2,
        "name": "seoiru",
        "description": "asadasd",
        "deleted_at": null,
        "created_at": "2018-12-13 11:57:59",
        "updated_at": "2018-12-13 11:57:59"
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/category/{category}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    category_id | integer |  required  | the ID of the category

<!-- END_3472a3f42a5b1dc76bb66947128507de -->

<!-- START_1db2d190ad189a02f319863757b20317 -->
## api/v1/category/{category}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/category/{category}"     -d "category_id"="25KvOSMtIs7J0nzQ" 
```

```javascript
const url = new URL("http://localhost/api/v1/category/{category}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "category_id": "25KvOSMtIs7J0nzQ",
})

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "name": "seoiru",
        "description": "asadasd",
        "updated_at": "2018-12-13 11:57:59",
        "created_at": "2018-12-13 11:57:59",
        "category_id": 2
    },
    "messages": null
}
```

### HTTP Request
`PUT api/v1/category/{category}`

`PATCH api/v1/category/{category}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    category_id | string |  required  | CategoryId.

<!-- END_1db2d190ad189a02f319863757b20317 -->

<!-- START_6a1a0789d7f1fd66545b46c16f81e028 -->
## api/v1/category/{category}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/category/{category}"     -d "category_id"="16" 
```

```javascript
const url = new URL("http://localhost/api/v1/category/{category}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "category_id": "16",
})

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": "null",
    "messages": null
}
```

### HTTP Request
`DELETE api/v1/category/{category}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    category_id | integer |  required  | the ID of the category

<!-- END_6a1a0789d7f1fd66545b46c16f81e028 -->

#contact_types Management

APIs for managing contact_types
<!-- START_fa75735f448ae7e9c047a7c729a1f61e -->
## api/v1/contact-types
> Example request:

```bash
curl -X POST "http://localhost/api/v1/contact-types"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/contact-types");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "total": 1,
        "rows": [
            {
                "contact_type_id": 2,
                "name": "435435dfgdf",
                "description": "234345dfg",
                "deleted_at": null,
                "created_at": "2018-12-14 03:53:50",
                "updated_at": "2018-12-14 03:53:50"
            }
        ]
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/contact-types`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_fa75735f448ae7e9c047a7c729a1f61e -->

<!-- START_ca311c19f137089735afee22ce7cdcca -->
## api/v1/contact-type
> Example request:

```bash
curl -X POST "http://localhost/api/v1/contact-type"     -d "name"="oKMK8OjLbrigMDGW" \
    -d "description"="idC4UloagxqmNo3j" 
```

```javascript
const url = new URL("http://localhost/api/v1/contact-type");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "oKMK8OjLbrigMDGW",
    "description": "idC4UloagxqmNo3j",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "name": "435435dfgdf",
        "description": "234345dfg",
        "updated_at": "2018-12-14 03:53:50",
        "created_at": "2018-12-14 03:53:50",
        "contact_type_id": 2
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/contact-type`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | name.
    description | string |  required  | description.

<!-- END_ca311c19f137089735afee22ce7cdcca -->

<!-- START_e152b8fd738244cfbb1290f7c083d98d -->
## api/v1/contact-type/{contact_type}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/contact-type/{contact_type}"     -d "contact_type_id"="11" 
```

```javascript
const url = new URL("http://localhost/api/v1/contact-type/{contact_type}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "contact_type_id": "11",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "contact_type_id": 2,
        "name": "435435dfgdf",
        "description": "234345dfg",
        "deleted_at": null,
        "created_at": "2018-12-14 03:53:50",
        "updated_at": "2018-12-14 03:53:50"
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/contact-type/{contact_type}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    contact_type_id | integer |  required  | the ID of the contact_type_id

<!-- END_e152b8fd738244cfbb1290f7c083d98d -->

<!-- START_ce70ff7e6571619c20d0e3b2bdea72cd -->
## api/v1/contact-type/{contact_type}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/contact-type/{contact_type}"     -d "name"="79jEVJjRyQsiLqKt" \
    -d "description"="kx6F6EWTiuRZipi5" \
    -d "client_id"="13" 
```

```javascript
const url = new URL("http://localhost/api/v1/contact-type/{contact_type}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "79jEVJjRyQsiLqKt",
    "description": "kx6F6EWTiuRZipi5",
    "client_id": "13",
})

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "contact_type_id": 2,
        "name": "234234",
        "description": "45",
        "deleted_at": null,
        "created_at": "2018-12-14 03:53:50",
        "updated_at": "2018-12-14 03:59:28"
    },
    "messages": null
}
```

### HTTP Request
`PUT api/v1/contact-type/{contact_type}`

`PATCH api/v1/contact-type/{contact_type}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | name.
    description | string |  required  | description.
    client_id | integer |  required  | client_id.

<!-- END_ce70ff7e6571619c20d0e3b2bdea72cd -->

<!-- START_9d73929dbe193eee1699c31d5f66aa8a -->
## api/v1/contact-type/{contact_type}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/contact-type/{contact_type}"     -d "contact_type_id"="11" 
```

```javascript
const url = new URL("http://localhost/api/v1/contact-type/{contact_type}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "contact_type_id": "11",
})

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": "null",
    "messages": null
}
```

### HTTP Request
`DELETE api/v1/contact-type/{contact_type}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    contact_type_id | integer |  required  | the ID of the contact_type

<!-- END_9d73929dbe193eee1699c31d5f66aa8a -->

#cookies Management

APIs for managing cookies
<!-- START_f8749d92690b7e44e25ec5c95e0195be -->
## api/v1/cookies
> Example request:

```bash
curl -X POST "http://localhost/api/v1/cookies"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/cookies");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "total": 2,
        "rows": [
            {
                "cookie_id": 1,
                "site_id": null,
                "status": 123232,
                "cookie_content": "asdasdasd",
                "deleted_at": null,
                "created_at": "2018-12-14 10:48:52",
                "updated_at": "2018-12-14 10:48:52"
            },
            {
                "cookie_id": 3,
                "site_id": 1,
                "status": 123232,
                "cookie_content": "asdasdasd",
                "deleted_at": null,
                "created_at": "2018-12-14 11:03:49",
                "updated_at": "2018-12-14 11:03:49"
            }
        ]
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/cookies`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_f8749d92690b7e44e25ec5c95e0195be -->

<!-- START_41501873947b436132d07c2388a25229 -->
## api/v1/cookie
> Example request:

```bash
curl -X POST "http://localhost/api/v1/cookie"     -d "status"="4Jgr1LiluhqQTnKw" \
    -d "cookie_content"="7Ykt4gqs7M1Lg4WZ" \
    -d "site_id"="1" 
```

```javascript
const url = new URL("http://localhost/api/v1/cookie");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "status": "4Jgr1LiluhqQTnKw",
    "cookie_content": "7Ykt4gqs7M1Lg4WZ",
    "site_id": "1",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "status": "123232",
        "cookie_content": "asdasdasd",
        "site_id": "1",
        "updated_at": "2018-12-14 06:47:20",
        "created_at": "2018-12-14 06:47:20",
        "cookie_id": 2
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/cookie`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    status | string |  required  | status.
    cookie_content | string |  required  | cookie_content.
    site_id | integer |  required  | site_id.

<!-- END_41501873947b436132d07c2388a25229 -->

<!-- START_41834a1eb9106a4a58fd99c0f9ab5c27 -->
## api/v1/cookie/{cookie}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/cookie/{cookie}"     -d "cookie_id"="13" 
```

```javascript
const url = new URL("http://localhost/api/v1/cookie/{cookie}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "cookie_id": "13",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "cookie_id": 1,
        "site_id": null,
        "status": 123232,
        "cookie_content": "asdasdasd",
        "deleted_at": null,
        "created_at": "2018-12-14 10:48:52",
        "updated_at": "2018-12-14 10:48:52"
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/cookie/{cookie}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    cookie_id | integer |  required  | the ID of the cookie

<!-- END_41834a1eb9106a4a58fd99c0f9ab5c27 -->

<!-- START_306d5ba05ed2ef61efc8b2b8edfbb604 -->
## api/v1/cookie/{cookie}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/cookie/{cookie}"     -d "status"="7MAahL4PlcYUVWYK" \
    -d "cookie_content"="gDGyPpRmbmdTQxWE" \
    -d "site_id"="3" \
    -d "cookie_id"="9" 
```

```javascript
const url = new URL("http://localhost/api/v1/cookie/{cookie}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "status": "7MAahL4PlcYUVWYK",
    "cookie_content": "gDGyPpRmbmdTQxWE",
    "site_id": "3",
    "cookie_id": "9",
})

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "status": "123232",
        "cookie_content": "asdasdasd",
        "site_id": "1",
        "updated_at": "2018-12-14 06:47:20",
        "created_at": "2018-12-14 06:47:20",
        "cookie_id": 2
    },
    "messages": null
}
```

### HTTP Request
`PUT api/v1/cookie/{cookie}`

`PATCH api/v1/cookie/{cookie}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    status | string |  required  | status.
    cookie_content | string |  required  | cookie_content.
    site_id | integer |  required  | site_id.
    cookie_id | integer |  required  | cookie_id, whose data to be updated.

<!-- END_306d5ba05ed2ef61efc8b2b8edfbb604 -->

<!-- START_ea80ce49654a15059aea2f1a9602c5ec -->
## api/v1/cookie/{cookie}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/cookie/{cookie}"     -d "cookie_id"="17" 
```

```javascript
const url = new URL("http://localhost/api/v1/cookie/{cookie}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "cookie_id": "17",
})

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": "null",
    "messages": null
}
```

### HTTP Request
`DELETE api/v1/cookie/{cookie}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    cookie_id | integer |  required  | the ID of the cookie

<!-- END_ea80ce49654a15059aea2f1a9602c5ec -->

#event_types Management

APIs for managing event_types
<!-- START_5c019dd8b3083d441b68025090a46042 -->
## api/v1/event-types
> Example request:

```bash
curl -X POST "http://localhost/api/v1/event-types"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/event-types");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "total": 1,
        "rows": [
            {
                "event_type_id": 1,
                "name": "asdasd",
                "description": "34234",
                "deleted_at": null,
                "created_at": "2018-12-17 12:03:20",
                "updated_at": "2018-12-17 12:03:20"
            }
        ]
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/event-types`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_5c019dd8b3083d441b68025090a46042 -->

<!-- START_51a0c0a77511cd36bea12734ef413e2a -->
## api/v1/event-type
> Example request:

```bash
curl -X POST "http://localhost/api/v1/event-type"     -d "name"="9hcPfOq76OXj7LaE" \
    -d "description"="LK7wcgZLwH6D9YF8" 
```

```javascript
const url = new URL("http://localhost/api/v1/event-type");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "9hcPfOq76OXj7LaE",
    "description": "LK7wcgZLwH6D9YF8",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "name": "asdasd",
        "description": "34234",
        "updated_at": "2018-12-17 12:03:20",
        "created_at": "2018-12-17 12:03:20",
        "event_type_id": 1
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/event-type`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | name.
    description | string |  required  | description.

<!-- END_51a0c0a77511cd36bea12734ef413e2a -->

<!-- START_b2caf964fa7d769fbe8ace9e99d18881 -->
## api/v1/event-type/{event_type}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/event-type/{event_type}"     -d "name"="Phuh9fJ6aUAMVxlm" \
    -d "description"="qXGTlkD4kvualxWM" \
    -d "event_type_id"="na9jra24jeQDKKW6" 
```

```javascript
const url = new URL("http://localhost/api/v1/event-type/{event_type}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "Phuh9fJ6aUAMVxlm",
    "description": "qXGTlkD4kvualxWM",
    "event_type_id": "na9jra24jeQDKKW6",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "name": "asdasd",
        "description": "34234",
        "updated_at": "2018-12-17 12:03:20",
        "created_at": "2018-12-17 12:03:20",
        "event_type_id": 1
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/event-type/{event_type}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | name.
    description | string |  required  | description.
    event_type_id | string |  required  | event_type_id, whose record to be seen.

<!-- END_b2caf964fa7d769fbe8ace9e99d18881 -->

<!-- START_a28d9d181b4f1f4e45cc4108584c1fa9 -->
## api/v1/event-type/{event_type}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/event-type/{event_type}"     -d "name"="nY0Zqzj9qVMlLqfj" \
    -d "description"="Kn5BpYcnIv2ir11E" \
    -d "event_type_id"="53gQQ5QyzH2E1DIy" 
```

```javascript
const url = new URL("http://localhost/api/v1/event-type/{event_type}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "nY0Zqzj9qVMlLqfj",
    "description": "Kn5BpYcnIv2ir11E",
    "event_type_id": "53gQQ5QyzH2E1DIy",
})

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "name": "asdasd",
        "description": "34234",
        "updated_at": "2018-12-17 12:03:20",
        "created_at": "2018-12-17 12:03:20",
        "event_type_id": 1
    },
    "messages": null
}
```

### HTTP Request
`PUT api/v1/event-type/{event_type}`

`PATCH api/v1/event-type/{event_type}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | name.
    description | string |  required  | description.
    event_type_id | string |  required  | event_type_id, whose record to be updated.

<!-- END_a28d9d181b4f1f4e45cc4108584c1fa9 -->

<!-- START_8de2ab1823fb49ba4a1991a485824a2f -->
## api/v1/event-type/{event_type}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/event-type/{event_type}"     -d "event_type_id"="15" 
```

```javascript
const url = new URL("http://localhost/api/v1/event-type/{event_type}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "event_type_id": "15",
})

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": "null",
    "messages": null
}
```

### HTTP Request
`DELETE api/v1/event-type/{event_type}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    event_type_id | integer |  required  | the ID of the event_type

<!-- END_8de2ab1823fb49ba4a1991a485824a2f -->

#events Management

APIs for managing events
<!-- START_40b2dca13b49082417bca22e110bb0d1 -->
## api/v1/events
> Example request:

```bash
curl -X POST "http://localhost/api/v1/events"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/events");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "total": 1,
        "rows": [
            {
                "event_id": 4,
                "tracker_id": 2,
                "tracker_cookie_id": 1,
                "url": "url",
                "post_id": 1,
                "event_time": "2018-12-17",
                "length": 12,
                "source": null,
                "exit": "true",
                "deleted_at": null,
                "created_at": "2018-12-17 07:52:23",
                "updated_at": "2018-12-17 07:52:23"
            }
        ]
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/events`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_40b2dca13b49082417bca22e110bb0d1 -->

<!-- START_4415426e755368b4fffcc32da833078f -->
## api/v1/event
> Example request:

```bash
curl -X POST "http://localhost/api/v1/event"     -d "tracker_id"="4" \
    -d "tracker_cookie_id"="7" \
    -d "url"="y9DOWCHhD5Y0q4mS" \
    -d "post_id"="4" \
    -d "event_time"="i65rKg9dUQJpzBAb" \
    -d "length"="13" \
    -d "exit"="JXXyLzxYXovM94lY" 
```

```javascript
const url = new URL("http://localhost/api/v1/event");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "tracker_id": "4",
    "tracker_cookie_id": "7",
    "url": "y9DOWCHhD5Y0q4mS",
    "post_id": "4",
    "event_time": "i65rKg9dUQJpzBAb",
    "length": "13",
    "exit": "JXXyLzxYXovM94lY",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "tracker_id": "2",
        "tracker_cookie_id": "1",
        "url": "url",
        "post_id": "1",
        "event_time": "2018-12-17 02:05:29",
        "length": "12",
        "exit": "true",
        "updated_at": "2018-12-17 07:52:23",
        "created_at": "2018-12-17 07:52:23",
        "event_id": 4
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/event`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    tracker_id | integer |  required  | tracker_id, a foreign key.
    tracker_cookie_id | integer |  required  | tracker_cookie_id, a foreign key.
    url | string |  required  | url.
    post_id | integer |  required  | post_id, a foreign key.
    event_time | date |  required  | event_time.
    length | integer |  required  | length.
    exit | string |  required  | exit.

<!-- END_4415426e755368b4fffcc32da833078f -->

<!-- START_dfff739168521000ce0d4e661684f3c6 -->
## api/v1/event/{event}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/event/{event}"     -d "event_id"="4" 
```

```javascript
const url = new URL("http://localhost/api/v1/event/{event}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "event_id": "4",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "tracker_id": "2",
        "tracker_cookie_id": "1",
        "url": "url",
        "post_id": "1",
        "event_time": "2018-12-17 02:05:29",
        "length": "12",
        "exit": "true",
        "updated_at": "2018-12-17 07:52:23",
        "created_at": "2018-12-17 07:52:23",
        "event_id": 4
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/event/{event}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    event_id | integer |  required  | event_id, a foreign key.

<!-- END_dfff739168521000ce0d4e661684f3c6 -->

<!-- START_db6f4119c863c62f4dbfb9f4cd77bc0c -->
## api/v1/event/{event}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/event/{event}"     -d "tracker_id"="2" \
    -d "tracker_cookie_id"="6" \
    -d "url"="wNAcJHHxNFaBw7Gr" \
    -d "post_id"="16" \
    -d "event_time"="2qn7Df92TT6oSnTZ" \
    -d "length"="2" \
    -d "exit"="IwkJpjN4oqPbPpyd" \
    -d "event_id"="0YVpkkHb1Mmu6nE4" 
```

```javascript
const url = new URL("http://localhost/api/v1/event/{event}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "tracker_id": "2",
    "tracker_cookie_id": "6",
    "url": "wNAcJHHxNFaBw7Gr",
    "post_id": "16",
    "event_time": "2qn7Df92TT6oSnTZ",
    "length": "2",
    "exit": "IwkJpjN4oqPbPpyd",
    "event_id": "0YVpkkHb1Mmu6nE4",
})

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "tracker_id": "2",
        "tracker_cookie_id": "1",
        "url": "url",
        "post_id": "1",
        "event_time": "2018-12-17 02:05:29",
        "length": "12",
        "exit": "true",
        "updated_at": "2018-12-17 07:52:23",
        "created_at": "2018-12-17 07:52:23",
        "event_id": 4
    },
    "messages": null
}
```

### HTTP Request
`PUT api/v1/event/{event}`

`PATCH api/v1/event/{event}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    tracker_id | integer |  required  | tracker_id, a foreign key.
    tracker_cookie_id | integer |  required  | tracker_cookie_id, a foreign key.
    url | string |  required  | url.
    post_id | integer |  required  | post_id, a foreign key.
    event_time | date |  required  | event_time.
    length | integer |  required  | length.
    exit | string |  required  | exit.
    event_id | string |  required  | event_id, whose record to be updated.

<!-- END_db6f4119c863c62f4dbfb9f4cd77bc0c -->

<!-- START_02c7e410c92784782b027cd32d55372d -->
## api/v1/event/{event}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/event/{event}"     -d "event_id"="6" 
```

```javascript
const url = new URL("http://localhost/api/v1/event/{event}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "event_id": "6",
})

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": "null",
    "messages": null
}
```

### HTTP Request
`DELETE api/v1/event/{event}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    event_id | integer |  required  | the ID of the event

<!-- END_02c7e410c92784782b027cd32d55372d -->

#general
<!-- START_66e08d3cc8222573018fed49e121e96d -->
## Show the application&#039;s login form.

> Example request:

```bash
curl -X GET -G "http://localhost/login" 
```

```javascript
const url = new URL("http://localhost/login");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
null
```

### HTTP Request
`GET login`


<!-- END_66e08d3cc8222573018fed49e121e96d -->

<!-- START_ba35aa39474cb98cfb31829e70eb8b74 -->
## Handle a login request to the application.

> Example request:

```bash
curl -X POST "http://localhost/login" 
```

```javascript
const url = new URL("http://localhost/login");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST login`


<!-- END_ba35aa39474cb98cfb31829e70eb8b74 -->

<!-- START_e65925f23b9bc6b93d9356895f29f80c -->
## Log the user out of the application.

> Example request:

```bash
curl -X POST "http://localhost/logout" 
```

```javascript
const url = new URL("http://localhost/logout");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST logout`


<!-- END_e65925f23b9bc6b93d9356895f29f80c -->

<!-- START_ff38dfb1bd1bb7e1aa24b4e1792a9768 -->
## Show the application registration form.

> Example request:

```bash
curl -X GET -G "http://localhost/register" 
```

```javascript
const url = new URL("http://localhost/register");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
null
```

### HTTP Request
`GET register`


<!-- END_ff38dfb1bd1bb7e1aa24b4e1792a9768 -->

<!-- START_d7aad7b5ac127700500280d511a3db01 -->
## Handle a registration request for the application.

> Example request:

```bash
curl -X POST "http://localhost/register" 
```

```javascript
const url = new URL("http://localhost/register");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST register`


<!-- END_d7aad7b5ac127700500280d511a3db01 -->

<!-- START_d72797bae6d0b1f3a341ebb1f8900441 -->
## Display the form to request a password reset link.

> Example request:

```bash
curl -X GET -G "http://localhost/password/reset" 
```

```javascript
const url = new URL("http://localhost/password/reset");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
null
```

### HTTP Request
`GET password/reset`


<!-- END_d72797bae6d0b1f3a341ebb1f8900441 -->

<!-- START_feb40f06a93c80d742181b6ffb6b734e -->
## Send a reset link to the given user.

> Example request:

```bash
curl -X POST "http://localhost/password/email" 
```

```javascript
const url = new URL("http://localhost/password/email");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST password/email`


<!-- END_feb40f06a93c80d742181b6ffb6b734e -->

<!-- START_e1605a6e5ceee9d1aeb7729216635fd7 -->
## Display the password reset view for the given token.

If no token is present, display the link request form.

> Example request:

```bash
curl -X GET -G "http://localhost/password/reset/{token}" 
```

```javascript
const url = new URL("http://localhost/password/reset/{token}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
null
```

### HTTP Request
`GET password/reset/{token}`


<!-- END_e1605a6e5ceee9d1aeb7729216635fd7 -->

<!-- START_cafb407b7a846b31491f97719bb15aef -->
## Reset the given user&#039;s password.

> Example request:

```bash
curl -X POST "http://localhost/password/reset" 
```

```javascript
const url = new URL("http://localhost/password/reset");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST password/reset`


<!-- END_cafb407b7a846b31491f97719bb15aef -->

<!-- START_cb859c8e84c35d7133b6a6c8eac253f8 -->
## Show the application dashboard.

> Example request:

```bash
curl -X GET -G "http://localhost/home" 
```

```javascript
const url = new URL("http://localhost/home");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET home`


<!-- END_cb859c8e84c35d7133b6a6c8eac253f8 -->

<!-- START_30059a09ef3f0284c40e4d06962ce08d -->
## Show the application dashboard.

> Example request:

```bash
curl -X GET -G "http://localhost/dashboard" 
```

```javascript
const url = new URL("http://localhost/dashboard");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET dashboard`


<!-- END_30059a09ef3f0284c40e4d06962ce08d -->

#magazines Management

APIs for managing magazines
<!-- START_33cee0d3cab960d2d2a49dee75572d35 -->
## api/v1/magazines
> Example request:

```bash
curl -X POST "http://localhost/api/v1/magazines"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/magazines");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/v1/magazines`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_33cee0d3cab960d2d2a49dee75572d35 -->

<!-- START_2adf5691ea678ee24513fc2ff8447738 -->
## api/v1/magazine
> Example request:

```bash
curl -X POST "http://localhost/api/v1/magazine"     -d "name"="mdkanINBg0TSXiqX" \
    -d "client_id"="5" \
    -d "api_key"="15" \
    -d "description"="ArGgC5Dekqks5zq2" \
    -d "logo"="0XjqVX92AwV6f2QV" 
```

```javascript
const url = new URL("http://localhost/api/v1/magazine");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "mdkanINBg0TSXiqX",
    "client_id": "5",
    "api_key": "15",
    "description": "ArGgC5Dekqks5zq2",
    "logo": "0XjqVX92AwV6f2QV",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "name": "last test",
        "api_key": "23233ewrwr",
        "client_id": "2",
        "description": "this is description",
        "logo": "magazine_logo_url",
        "updated_at": "2018-12-13 10:46:08",
        "created_at": "2018-12-13 10:46:08",
        "magazine_id": 13
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/magazine`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | name.
    client_id | integer |  required  | client_id, is a foreign key.
    api_key | integer |  required  | api_key.
    description | string |  optional  | optional Description of Magazine.
    logo | file |  optional  | optional Logo of Magazine.

<!-- END_2adf5691ea678ee24513fc2ff8447738 -->

<!-- START_395cd9543326c8a7932f9dc3d628b229 -->
## api/v1/magazine/{magazine}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/magazine/{magazine}"     -d "magazine_id"="16" 
```

```javascript
const url = new URL("http://localhost/api/v1/magazine/{magazine}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "magazine_id": "16",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "magazine_id": 13,
        "client_id": 2,
        "api_key": "23233ewrwr",
        "name": "last test",
        "description": "this is description",
        "logo": null,
        "deleted_at": null,
        "created_at": "2018-12-13 10:46:08",
        "updated_at": "2018-12-13 10:46:08"
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/magazine/{magazine}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    magazine_id | integer |  required  | the ID of the magazine

<!-- END_395cd9543326c8a7932f9dc3d628b229 -->

<!-- START_831f3fca2176a7915cce2708d23f818f -->
## api/v1/magazine/{magazine}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/magazine/{magazine}"     -d "magazine_id"="3" \
    -d "name"="Tq85vQxhfGXBOyk8" \
    -d "api_key"="gioh19emIiFQST12" \
    -d "client_id"="2" \
    -d "description"="0w1zNnaopBcieFQH" \
    -d "logo"="fvtH59p7JgTi1cC0" 
```

```javascript
const url = new URL("http://localhost/api/v1/magazine/{magazine}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "magazine_id": "3",
    "name": "Tq85vQxhfGXBOyk8",
    "api_key": "gioh19emIiFQST12",
    "client_id": "2",
    "description": "0w1zNnaopBcieFQH",
    "logo": "fvtH59p7JgTi1cC0",
})

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "magazine_id": 13,
        "client_id": "2",
        "api_key": "454545345",
        "name": "magazinename23456546546",
        "description": "description34",
        "logo": null,
        "deleted_at": null,
        "created_at": "2018-12-13 10:46:08",
        "updated_at": "2018-12-13 10:51:08"
    },
    "messages": null
}
```

### HTTP Request
`PUT api/v1/magazine/{magazine}`

`PATCH api/v1/magazine/{magazine}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    magazine_id | integer |  required  | Id of magazine which needs to updated.
    name | string |  required  | The title of the magazine.
    api_key | string |  required  | The title of the magazine.
    client_id | integer |  optional  | required, is foreign key.
    description | string |  required  | for the description of the magazine
    logo | file |  optional  | optional for the logo of the magazine

<!-- END_831f3fca2176a7915cce2708d23f818f -->

<!-- START_f345dc808c94c6c2ad8babe972d74892 -->
## api/v1/magazine/{magazine}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/magazine/{magazine}"     -d "magazine_id"="20" 
```

```javascript
const url = new URL("http://localhost/api/v1/magazine/{magazine}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "magazine_id": "20",
})

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": "null",
    "messages": null
}
```

### HTTP Request
`DELETE api/v1/magazine/{magazine}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    magazine_id | integer |  required  | the ID of the magazine

<!-- END_f345dc808c94c6c2ad8babe972d74892 -->

#post_category Management

APIs for managing post_category
<!-- START_e1a23ed1a0367ddf3bb27f13aa568e8d -->
## api/v1/post-categories
> Example request:

```bash
curl -X POST "http://localhost/api/v1/post-categories"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/post-categories");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "total": 1,
        "rows": [
            {
                "post_category_id": 1,
                "category_id": 1,
                "post_id": 1,
                "value": 435345,
                "created_at": "2018-12-17 09:00:19",
                "updated_at": "2018-12-17 09:00:19"
            }
        ]
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/post-categories`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_e1a23ed1a0367ddf3bb27f13aa568e8d -->

<!-- START_c296b792d8ffc78a0dd8abc4a491c32c -->
## api/v1/post-category
> Example request:

```bash
curl -X POST "http://localhost/api/v1/post-category"     -d "category_id"="15" \
    -d "post_id"="18" \
    -d "value"="12" 
```

```javascript
const url = new URL("http://localhost/api/v1/post-category");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "category_id": "15",
    "post_id": "18",
    "value": "12",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "category_id": "1",
        "post_id": "1",
        "value": "435345",
        "updated_at": "2018-12-17 09:00:19",
        "created_at": "2018-12-17 09:00:19",
        "post_category_id": 1
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/post-category`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    category_id | integer |  required  | category_id, a foreign key.
    post_id | integer |  required  | post_id, a foreign key.
    value | integer |  required  | value.

<!-- END_c296b792d8ffc78a0dd8abc4a491c32c -->

<!-- START_9f5978e6431f4f6f327e1ed3c191fc51 -->
## api/v1/post-category/{post_category}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/post-category/{post_category}"     -d "post_category_id"="15" 
```

```javascript
const url = new URL("http://localhost/api/v1/post-category/{post_category}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "post_category_id": "15",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "category_id": "1",
        "post_id": "1",
        "value": "435345",
        "updated_at": "2018-12-17 09:00:19",
        "created_at": "2018-12-17 09:00:19",
        "post_category_id": 1
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/post-category/{post_category}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    post_category_id | integer |  required  | post_category_id, whose record to be retrieved.

<!-- END_9f5978e6431f4f6f327e1ed3c191fc51 -->

<!-- START_e970324fe7e01e89ef84f64aad897d45 -->
## api/v1/post-category/{post_category}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/post-category/{post_category}"     -d "category_id"="8" \
    -d "post_id"="4" \
    -d "value"="3" 
```

```javascript
const url = new URL("http://localhost/api/v1/post-category/{post_category}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "category_id": "8",
    "post_id": "4",
    "value": "3",
})

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "category_id": "1",
        "post_id": "1",
        "value": "435345",
        "updated_at": "2018-12-17 09:00:19",
        "created_at": "2018-12-17 09:00:19",
        "post_category_id": 1
    },
    "messages": null
}
```

### HTTP Request
`PUT api/v1/post-category/{post_category}`

`PATCH api/v1/post-category/{post_category}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    category_id | integer |  required  | category_id, a foreign key.
    post_id | integer |  required  | post_id, a foreign key.
    value | integer |  required  | value.

<!-- END_e970324fe7e01e89ef84f64aad897d45 -->

<!-- START_e55576a7b13a99f82f1e771088fa2b86 -->
## api/v1/post-category/{post_category}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/post-category/{post_category}"     -d "post_category_id"="20" 
```

```javascript
const url = new URL("http://localhost/api/v1/post-category/{post_category}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "post_category_id": "20",
})

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": "null",
    "messages": null
}
```

### HTTP Request
`DELETE api/v1/post-category/{post_category}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    post_category_id | integer |  required  | the ID of the post_category

<!-- END_e55576a7b13a99f82f1e771088fa2b86 -->

#posts Management

APIs for managing posts
APIs for managing posts
<!-- START_8c8dee3dc083fa8a3bbfd25342a7c1b6 -->
## api/v1/posts
> Example request:

```bash
curl -X POST "http://localhost/api/v1/posts"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/posts");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/v1/posts`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_8c8dee3dc083fa8a3bbfd25342a7c1b6 -->

<!-- START_7a2421ca325308a8d8e5aa4d74af0a50 -->
## api/v1/post
> Example request:

```bash
curl -X POST "http://localhost/api/v1/post"     -d "url"="8mO3nZAgNMVx4jCi" \
    -d "site_id"="9" \
    -d "value"="16" 
```

```javascript
const url = new URL("http://localhost/api/v1/post");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "url": "8mO3nZAgNMVx4jCi",
    "site_id": "9",
    "value": "16",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "url": "url",
        "site_id": "2",
        "value": "234",
        "updated_at": "2018-12-17 06:30:48",
        "created_at": "2018-12-17 06:30:48",
        "post_id": 1
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/post`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    url | string |  required  | url.
    site_id | integer |  required  | site_id, is a foreign key.
    value | integer |  required  | value.

<!-- END_7a2421ca325308a8d8e5aa4d74af0a50 -->

<!-- START_a2b020ecbcec5a240f6f52a9de3827dd -->
## api/v1/post/{post}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/post/{post}"     -d "post_id"="11" 
```

```javascript
const url = new URL("http://localhost/api/v1/post/{post}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "post_id": "11",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "url": "url",
        "site_id": "2",
        "value": "234",
        "updated_at": "2018-12-17 06:30:48",
        "created_at": "2018-12-17 06:30:48",
        "post_id": 1
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/post/{post}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    post_id | integer |  required  | post_id.

<!-- END_a2b020ecbcec5a240f6f52a9de3827dd -->

<!-- START_cb03b58da8ff8f088315dc41b07d50a8 -->
## api/v1/post/{post}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/post/{post}"     -d "url"="SHaqmcp4a4S6UiGX" \
    -d "site_id"="9" \
    -d "value"="14" \
    -d "post_id"="14" 
```

```javascript
const url = new URL("http://localhost/api/v1/post/{post}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "url": "SHaqmcp4a4S6UiGX",
    "site_id": "9",
    "value": "14",
    "post_id": "14",
})

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "url": "url",
        "site_id": "2",
        "value": "234",
        "updated_at": "2018-12-17 06:30:48",
        "created_at": "2018-12-17 06:30:48",
        "post_id": 1
    },
    "messages": null
}
```

### HTTP Request
`PUT api/v1/post/{post}`

`PATCH api/v1/post/{post}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    url | string |  required  | url.
    site_id | integer |  required  | site_id, is a foreign key.
    value | integer |  required  | value.
    post_id | integer |  required  | post_id, which to be updated.

<!-- END_cb03b58da8ff8f088315dc41b07d50a8 -->

<!-- START_f4258fbea3dcb1ea51ad074c2979e882 -->
## api/v1/post/{post}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/post/{post}"     -d "post_id"="1" 
```

```javascript
const url = new URL("http://localhost/api/v1/post/{post}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "post_id": "1",
})

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": "null",
    "messages": null
}
```

### HTTP Request
`DELETE api/v1/post/{post}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    post_id | integer |  required  | the ID of the post

<!-- END_f4258fbea3dcb1ea51ad074c2979e882 -->

#sites Management

APIs for managing sites
<!-- START_f3c4cfd076c7d491e1373000d9975051 -->
## api/v1/sites
> Example request:

```bash
curl -X POST "http://localhost/api/v1/sites"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/sites");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/v1/sites`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_f3c4cfd076c7d491e1373000d9975051 -->

<!-- START_acf148dc24caade29052880793eed0af -->
## api/v1/site
> Example request:

```bash
curl -X POST "http://localhost/api/v1/site"     -d "name"="G3qkUpJuQf1FMlIa" \
    -d "url"="bPw8eofWXrRl5dIM" \
    -d "magazine_id"="6" \
    -d "status"="YZXuYCwgX6knQGMU" 
```

```javascript
const url = new URL("http://localhost/api/v1/site");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "G3qkUpJuQf1FMlIa",
    "url": "bPw8eofWXrRl5dIM",
    "magazine_id": "6",
    "status": "YZXuYCwgX6knQGMU",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "name": "Suba",
        "url": "url",
        "magazine_id": "2",
        "status": "true",
        "updated_at": "2018-12-17 06:17:57",
        "created_at": "2018-12-17 06:17:57",
        "site_id": 2
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/site`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | name.
    url | string |  required  | url.
    magazine_id | integer |  required  | magazine_id, is a foreign key.
    status | string |  required  | status of site.

<!-- END_acf148dc24caade29052880793eed0af -->

<!-- START_1c0a526b504ba0a8747367f60ffc0977 -->
## api/v1/site/{site}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/site/{site}"     -d "site_id"="7" 
```

```javascript
const url = new URL("http://localhost/api/v1/site/{site}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "site_id": "7",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "name": "Suba",
        "url": "url",
        "magazine_id": "2",
        "status": "true",
        "updated_at": "2018-12-17 06:17:57",
        "created_at": "2018-12-17 06:17:57",
        "site_id": 2
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/site/{site}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    site_id | integer |  required  | the ID of the site

<!-- END_1c0a526b504ba0a8747367f60ffc0977 -->

<!-- START_ecf9447dd2b5181bafe1d04dc5c36ad7 -->
## api/v1/site/{site}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/site/{site}"     -d "name"="PTxsjt9JyYAspdhy" \
    -d "url"="Mp8hiQsl5o50HIKL" \
    -d "magazine_id"="13" \
    -d "status"="pLPrJLw7TWP338r5" \
    -d "site_id"="7E6vIeLdqqDvT8Qw" 
```

```javascript
const url = new URL("http://localhost/api/v1/site/{site}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "name": "PTxsjt9JyYAspdhy",
    "url": "Mp8hiQsl5o50HIKL",
    "magazine_id": "13",
    "status": "pLPrJLw7TWP338r5",
    "site_id": "7E6vIeLdqqDvT8Qw",
})

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "name": "Suba",
        "url": "url",
        "magazine_id": "2",
        "status": "true",
        "updated_at": "2018-12-17 06:17:57",
        "created_at": "2018-12-17 06:17:57",
        "site_id": 2
    },
    "messages": null
}
```

### HTTP Request
`PUT api/v1/site/{site}`

`PATCH api/v1/site/{site}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | name.
    url | string |  required  | url.
    magazine_id | integer |  required  | magazine_id, is a foreign key.
    status | string |  required  | status of site.
    site_id | string |  required  | site_id of site, whose record to be updated.

<!-- END_ecf9447dd2b5181bafe1d04dc5c36ad7 -->

<!-- START_5e57b09e98f477cd7f01c9b83eb85611 -->
## api/v1/site/{site}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/site/{site}"     -d "site_id"="13" 
```

```javascript
const url = new URL("http://localhost/api/v1/site/{site}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "site_id": "13",
})

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": "null",
    "messages": null
}
```

### HTTP Request
`DELETE api/v1/site/{site}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    site_id | integer |  required  | the ID of the site

<!-- END_5e57b09e98f477cd7f01c9b83eb85611 -->

#subscribers Management

APIs for managing subscribers
<!-- START_6dc4d4e7053e17c0807b7ea8ebe4e03b -->
## api/v1/subscribers
> Example request:

```bash
curl -X POST "http://localhost/api/v1/subscribers"     -d "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}"="{}" 
```

```javascript
const url = new URL("http://localhost/api/v1/subscribers");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "{&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}}": "{}",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "total": 1,
        "rows": [
            {
                "subscriber_id": 2,
                "tracker_id": 2,
                "subscription_id": 3,
                "start_date": "2018-12-17",
                "end_date": "2018-12-17",
                "source_id": 1,
                "status": 123,
                "price": 22222,
                "start_code": 1,
                "stop_code": 1,
                "name": null,
                "network_admin": null,
                "deleted_at": null,
                "created_at": "2018-12-17 11:16:24",
                "updated_at": "2018-12-17 11:21:03"
            }
        ]
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/subscribers`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    {&quot;page&quot;:1,&quot;limit&quot;:5,&quot;limitOptions&quot;:[5,10,15,20],&quot;search&quot;:{&quot;field&quot;:&quot;name&quot;,&quot;value&quot;:&quot;&quot;}} | object |  required  | name.

<!-- END_6dc4d4e7053e17c0807b7ea8ebe4e03b -->

<!-- START_01c7e320e52fd9b39bf8e258236e49e3 -->
## api/v1/subscriber
> Example request:

```bash
curl -X POST "http://localhost/api/v1/subscriber"     -d "tracker_id"="3" \
    -d "subscription_id"="11" \
    -d "start_date"="uAFkH7bLh4Zi3Q2A" \
    -d "end_date"="9Xv8Ij1SIXfE2eoe" \
    -d "source_id"="1" \
    -d "status"="ydUpyc6PZMEYdiSI" \
    -d "price"="15" \
    -d "start_code"="8" \
    -d "stop_code"="12" 
```

```javascript
const url = new URL("http://localhost/api/v1/subscriber");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "tracker_id": "3",
    "subscription_id": "11",
    "start_date": "uAFkH7bLh4Zi3Q2A",
    "end_date": "9Xv8Ij1SIXfE2eoe",
    "source_id": "1",
    "status": "ydUpyc6PZMEYdiSI",
    "price": "15",
    "start_code": "8",
    "stop_code": "12",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "tracker_id": "2",
        "subscription_id": "3",
        "start_date": "2018-12-17 06:15:07",
        "end_date": "2018-12-17 06:15:07",
        "source_id": "1",
        "status": "123",
        "price": "123123",
        "start_code": "1",
        "stop_code": "1",
        "updated_at": "2018-12-17 11:16:24",
        "created_at": "2018-12-17 11:16:24",
        "subscriber_id": 2
    },
    "messages": null
}
```

### HTTP Request
`POST api/v1/subscriber`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    tracker_id | integer |  required  | tracker_id, a foreign key.
    subscription_id | integer |  required  | subscription_id, a foreign key.
    start_date | date |  required  | start_date.
    end_date | date |  required  | end_date.
    source_id | integer |  required  | source_id.
    status | string |  required  | status.
    price | integer |  required  | price.
    start_code | integer |  required  | start_code.
    stop_code | integer |  required  | stop_code.

<!-- END_01c7e320e52fd9b39bf8e258236e49e3 -->

<!-- START_9b3899a50dfc51ccfb5a380b43da90d4 -->
## api/v1/subscriber/{subscriber}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/subscriber/{subscriber}"     -d "tracker_id"="10" \
    -d "subscription_id"="1" \
    -d "start_date"="fgtWn3yDh8VFLsEF" \
    -d "end_date"="m0InlWVwx2efVpYi" \
    -d "source_id"="2" \
    -d "status"="t7y4id31kyHMpLoX" \
    -d "price"="20" \
    -d "start_code"="11" \
    -d "stop_code"="9" \
    -d "subscriber_id"="19" 
```

```javascript
const url = new URL("http://localhost/api/v1/subscriber/{subscriber}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "tracker_id": "10",
    "subscription_id": "1",
    "start_date": "fgtWn3yDh8VFLsEF",
    "end_date": "m0InlWVwx2efVpYi",
    "source_id": "2",
    "status": "t7y4id31kyHMpLoX",
    "price": "20",
    "start_code": "11",
    "stop_code": "9",
    "subscriber_id": "19",
})

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "tracker_id": "2",
        "subscription_id": "3",
        "start_date": "2018-12-17 06:15:07",
        "end_date": "2018-12-17 06:15:07",
        "source_id": "1",
        "status": "123",
        "price": "123123",
        "start_code": "1",
        "stop_code": "1",
        "updated_at": "2018-12-17 11:16:24",
        "created_at": "2018-12-17 11:16:24",
        "subscriber_id": 2
    },
    "messages": null
}
```

### HTTP Request
`GET api/v1/subscriber/{subscriber}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    tracker_id | integer |  required  | tracker_id, a foreign key.
    subscription_id | integer |  required  | subscription_id, a foreign key.
    start_date | date |  required  | start_date.
    end_date | date |  required  | end_date.
    source_id | integer |  required  | source_id.
    status | string |  required  | status.
    price | integer |  required  | price.
    start_code | integer |  required  | start_code.
    stop_code | integer |  required  | stop_code.
    subscriber_id | integer |  required  | subscriber_id, whose record to be seen.

<!-- END_9b3899a50dfc51ccfb5a380b43da90d4 -->

<!-- START_e780a105a9d08fbe37947e4a92aa7462 -->
## api/v1/subscriber/{subscriber}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/subscriber/{subscriber}"     -d "tracker_id"="6" \
    -d "subscription_id"="11" \
    -d "start_date"="0ENGBavMN2BwfnvB" \
    -d "end_date"="U0bJuvQTwPs73DGB" \
    -d "source_id"="9" \
    -d "status"="BREVQAATPDTtJOWF" \
    -d "price"="3" \
    -d "start_code"="5" \
    -d "stop_code"="5" \
    -d "subscriber_id"="12" 
```

```javascript
const url = new URL("http://localhost/api/v1/subscriber/{subscriber}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "tracker_id": "6",
    "subscription_id": "11",
    "start_date": "0ENGBavMN2BwfnvB",
    "end_date": "U0bJuvQTwPs73DGB",
    "source_id": "9",
    "status": "BREVQAATPDTtJOWF",
    "price": "3",
    "start_code": "5",
    "stop_code": "5",
    "subscriber_id": "12",
})

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": {
        "tracker_id": "2",
        "subscription_id": "3",
        "start_date": "2018-12-17 06:15:07",
        "end_date": "2018-12-17 06:15:07",
        "source_id": "1",
        "status": "123",
        "price": "123123",
        "start_code": "1",
        "stop_code": "1",
        "updated_at": "2018-12-17 11:16:24",
        "created_at": "2018-12-17 11:16:24",
        "subscriber_id": 2
    },
    "messages": null
}
```

### HTTP Request
`PUT api/v1/subscriber/{subscriber}`

`PATCH api/v1/subscriber/{subscriber}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    tracker_id | integer |  required  | tracker_id, a foreign key.
    subscription_id | integer |  required  | subscription_id, a foreign key.
    start_date | date |  required  | start_date.
    end_date | date |  required  | end_date.
    source_id | integer |  required  | source_id.
    status | string |  required  | status.
    price | integer |  required  | price.
    start_code | integer |  required  | start_code.
    stop_code | integer |  required  | stop_code.
    subscriber_id | integer |  required  | subscriber_id, whose record to be updated.

<!-- END_e780a105a9d08fbe37947e4a92aa7462 -->

<!-- START_ac8d9bda52202e06eb74ff04558036a5 -->
## api/v1/subscriber/{subscriber}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/subscriber/{subscriber}"     -d "Subscriber_id"="2" 
```

```javascript
const url = new URL("http://localhost/api/v1/subscriber/{subscriber}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "Subscriber_id": "2",
})

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": "success",
    "result": "null",
    "messages": null
}
```

### HTTP Request
`DELETE api/v1/subscriber/{subscriber}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    Subscriber_id | integer |  required  | the ID of the Subscriber

<!-- END_ac8d9bda52202e06eb74ff04558036a5 -->


