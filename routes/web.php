<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




//
Route::get('/', function () {
    return redirect('dashboard');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/api/v1/clients', 'Api\V1\ClientController@index')->name('client.index')->middleware('auth');
Route::post('/api/v1/clients/auto-complete','Api\V1\ClientController@autocomplete')->name('clients.autocomplete')->middleware('auth');
Route::post('/api/v1/magazines/client', 'Api\V1\MagazineController@get_magazines_by_client')->name('magazines.client');
Route::resource('/api/v1/client', 'Api\V1\ClientController',['except' => ['index','create', 'edit']])->middleware('auth');

Route::post('/api/v1/users', 'Api\V1\UserController@index')->name('user.index')->middleware('auth');
Route::post('/api/v1/users/client', 'Api\V1\UserController@get_users_by_client')->name('users.client');
Route::resource('/api/v1/user', 'Api\V1\UserController',['except' => ['index','create', 'edit']])->middleware('auth');

Route::post('/api/v1/magazines', 'Api\V1\MagazineController@index')->name('magazine.index')->middleware('auth');
Route::resource('/api/v1/magazine', 'Api\V1\MagazineController',['except' => ['index','create', 'edit']])->middleware('auth');

Route::get('/dashboard', 'HomeController@index')->name('dashboard');

/*
* routes that redirect to vuejs default component
*
*/
Route::get('{slug}/list',function(){
    return view('home',['userInfo' => ['userID'=>\Auth::User()->user_id,'userName'=>\Auth::User()->name,'email'=>\Auth::User()->email]]);
})->middleware('auth');

Route::get('{slug}/{id}/details',function(){
    return view('home',['userInfo' => ['userID'=>\Auth::User()->user_id,'userName'=>\Auth::User()->name,'email'=>\Auth::User()->email]]);
})->middleware('auth');
